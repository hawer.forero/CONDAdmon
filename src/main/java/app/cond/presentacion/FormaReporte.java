package app.cond.presentacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import app.cond.entidades.Aspirante;
import app.cond.entidades.Concurso;
import app.cond.entidades.Convocatoria;
import app.cond.entidades.ExperienciaDocente;
import app.cond.entidades.ExperienciaProfesional;
import app.cond.entidades.Facultad;
import app.cond.entidades.FormacionAcademica;
import app.cond.entidades.InformacionPersonal;
import app.cond.entidades.Inscripcion;
import app.cond.entidades.ProductividadIntelectual;
import app.cond.entidades.SegundaLengua;
import app.cond.entidades.UnidadAcademica;
import app.cond.negocio.IConcursoBean;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@ManagedBean
@RequestScoped
@ViewScoped
public class FormaReporte {
	
	@EJB 
	IConcursoBean servicio;
	private List<Inscripcion> inscripciones;
	private Convocatoria convocatoria;
	private List<Concurso> facultades;
	private List<Concurso> unidades;
	private List<Concurso> concursos;
	
	public String generarActa(String idConvocatoria) throws Exception
	{
		this.convocatoria = servicio.buscarConvocatoria(idConvocatoria);
		this.inscripciones = servicio.buscarInscripcionesPorConvocatoria(convocatoria);
		this.facultades = servicio.agruparConcursosPorFacultad(convocatoria);
		this.unidades = servicio.agruparConcursosPorUnidad(convocatoria);
		this.concursos = servicio.buscarConcursosPorConvocatoria(convocatoria);
		int total = 0;
		
		FacesContext facesContext= FacesContext.getCurrentInstance();
	    HttpServletResponse response=(HttpServletResponse)facesContext.getExternalContext().getResponse();
	    facesContext.responseComplete(); 
	    
	    Document document = new Document(PageSize.LETTER);
	    try
	    {
	    	response.setContentType("application/pdf");
	    
	    	PdfWriter.getInstance(document, response.getOutputStream());
	    	
	    	document.open();
	    	Font bold= new Font();
	    	bold.setSize(12);
	    	bold.setStyle(1);
	    	Font fuente= new Font();
	    	fuente.setSize(12);
	    	Font tfuente= new Font();
	    	tfuente.setSize(10);
	    	Font tbold= new Font();
	    	tbold.setSize(10);
	    	tbold.setStyle(1);
	    	
	    	//ENCABEZADO
	        Paragraph titulo = new Paragraph("UNIVERSIDAD DE LOS LLANOS", bold);
	        titulo.setAlignment(1);
	        document.add(titulo);
	        
	        Paragraph titulo2 = new Paragraph("Vicerrectoría Académica", fuente);
	        titulo2.setAlignment(1);
	        document.add(titulo2);
	      
	        document.add(new Paragraph(" "));
	        Paragraph titulo3 = new Paragraph("ACTA DE CIERRE RECEPCIÓN HOJAS DE VIDA EN EL MARCO",bold);
	        titulo3.setAlignment(1);
	        document.add(titulo3);
	        Paragraph titulo4 = new Paragraph("DE LA CONVOCATORIA "+idConvocatoria, bold);
	        titulo4.setAlignment(1);
	        document.add(titulo4);
	        
	        document.add(new Paragraph(" ")); 
	        Paragraph des = new Paragraph("En Villavicencio siendo las (    :       ). Del día  _______ (   ) de __________ de __________, se da por terminada la recepción de hojas de vida. La siguiente es la relación de hojas de vida recibidas hasta la fecha");
	        des.setAlignment(3);
	        document.add(des);
	        
	        
	        for (int i= 0; i<facultades.size();i++)
	        {
	        	Integer x = i+1;
	        	document.add(new Paragraph(" ")); 
	        	Paragraph fac = new Paragraph(x+". "+facultades.get(i).getUnidadAcademica().getFacultad().getNombre(),bold);
	        	document.add(fac);	
	        	int y = 0;
	        	for(int j=0;j<unidades.size();j++)
	        	{
	        		
	        		if(facultades.get(i).getUnidadAcademica().getFacultad().getIdFacultad()==unidades.get(j).getUnidadAcademica().getFacultad().getIdFacultad())
	        		{
	        			y++;
	        			document.add(new Paragraph(" ")); 
	        			Paragraph uni = new Paragraph("  "+x+"."+y+"  "+unidades.get(j).getUnidadAcademica().getNombre(),tbold);
	    	        	document.add(uni);
	    	        	for(int k =0; k<concursos.size();k++)
	    	        	{
	    	        		if(unidades.get(j).getUnidadAcademica().getIdUnidadAcademica() == concursos.get(k).getUnidadAcademica().getIdUnidadAcademica())
	    	        		{
	    	        			document.add(new Paragraph(" "));
	    	        			Paragraph con = new Paragraph(concursos.get(k).getIdConcurso(),bold);
	    	        			con.setAlignment(1);
	    	    	        	document.add(con);
	    	    	        	
	    	    	        	Paragraph no = new Paragraph("No", tbold);
	    	    	    	    Paragraph nom = new Paragraph("NOMBRES Y APELLIDOS", tbold);
	    	    	    	    PdfPTable tabla1 = new PdfPTable(2);
	    	    	    	    tabla1.setWidthPercentage(new float[] {40,350},PageSize.A4);
	    	    	    	    PdfPCell celda =new PdfPCell (no);
	    	    	    	    celda.setPadding(4);
	    	    	    	    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	    	    	    tabla1.addCell(celda);
	    	    	    	    celda = new PdfPCell (nom);
	    	    	    	    celda.setPadding(4);
	    	    	    	    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	    	    	    tabla1.addCell(celda);
	    	    	    	    
	    	    	    	    int subT=0;   
	    	    	    	    for(int l=0; l<inscripciones.size();l++)
		    	    	        {
	    	    	    	    	
	    	    	    	    		if(concursos.get(k).getIdConcurso().equals(inscripciones.get(l).getConcurso().getIdConcurso()))
		    	    	    	    	{
	    	    	    	    			
	    	    	    	    			
	    	    	    	    			if(inscripciones.get(l).getRadico().equals("SI"))
	    	    	    	    	    	{
	    	    	    	    				subT++;
	    	    	    	    				celda = new PdfPCell (new Paragraph(""+subT));
	    	    	    	    				celda.setPadding(4);
	    	    	    	    				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	    	    	    				tabla1.addCell(celda);
		    	    	    	    
	    	    	    	    				celda = new PdfPCell (new Paragraph(inscripciones.get(l).getAspirante().getNombre()+" "+inscripciones.get(l).getAspirante().getApellido1()+" "+inscripciones.get(l).getAspirante().getApellido2(),tfuente));
	    	    	    	    				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	    	    	    				celda.setPadding(4);
	    	    	    	    				tabla1.addCell(celda);
	    	    	    	    				
			    	    		 	        }
	    	    	    	    	    	
	    	    	    	    	    	
		    	    	    	    	}
	    	    	    	    		
	    	    	    	    }
	    	    	    	    if(subT<=0)
	    	    	    	    {
	    	    	    	    	celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	    	    		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	    		 	        celda.setColspan(2);
	    	    		 	        tabla1.addCell(celda);
	    	    	    	    }
	    	    	    	    document.add(tabla1);
	    	    	    	    Paragraph p = new Paragraph("Total: "+subT,bold);
	    	    	    	    p.setAlignment(1);
	    	    	        	document.add(p);
	    	    	        	
	    	    	        	total = total + subT;
	    	    	        	
	    	        		}
	    	        	}
	        		}	
	        	}
		        
	        }
	        //Paragraph p = new Paragraph("TOTAL Inscripciones "+inscripciones.size()+"",bold);
        	//document.add(p);	
	        document.add(new Paragraph(" ")); 
	        Paragraph t = new Paragraph("En total se recibieron ("+total+") hojas de vida");
	        des.setAlignment(3);
	        document.add(t);
	        
	     //   document.add(new Paragraph(" ")); 
	       // Paragraph e = new Paragraph("En constancia firman:");
	        //des.setAlignment(3);
	        //document.add(e);
	      
	    }catch(Exception e)
    {
    	e.printStackTrace();
    }
    document.close();
    return "login";   
	        
	    }
	public FormaReporte(){
		super();
	}
	public List<Inscripcion> getInscripciones() {
		return inscripciones;
	}
	public void setInscripciones(List<Inscripcion> inscripciones) {
		this.inscripciones = inscripciones;
	}
	public Convocatoria getConvocatoria() {
		return convocatoria;
	}
	public void setConvocatoria(Convocatoria convocatoria) {
		this.convocatoria = convocatoria;
	}
	public List<Concurso> getFacultades() {
		return facultades;
	}
	public void setFacultades(List<Concurso> facultades) {
		this.facultades = facultades;
	}
	public List<Concurso> getUnidades() {
		return unidades;
	}
	public void setUnidades(List<Concurso> unidades) {
		this.unidades = unidades;
	}
	public List<Concurso> getConcursos() {
		return concursos;
	}
	public void setConcursos(List<Concurso> concursos) {
		this.concursos = concursos;
	}
    
	
   
}

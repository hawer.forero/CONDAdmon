package app.cond.presentacion;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import app.cond.entidades.Aspirante;
import app.cond.entidades.ExperienciaDocente;
import app.cond.entidades.ExperienciaProfesional;
import app.cond.entidades.FormacionAcademica;
import app.cond.entidades.InformacionPersonal;
import app.cond.entidades.Inscripcion;
import app.cond.entidades.ProductividadIntelectual;
import app.cond.entidades.SegundaLengua;
import app.cond.negocio.IConcursoBean;

import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@ManagedBean
@RequestScoped
@ViewScoped
public class FormaPdf {
	
	@EJB 
	IConcursoBean servicio;
	private Aspirante aspirante;
	private InformacionPersonal infoPersonal;
	private List<FormacionAcademica> formAcademica = new ArrayList<FormacionAcademica>();
	private List<SegundaLengua> segundaLengua = new ArrayList<SegundaLengua>();
	private List<ExperienciaDocente> experienciaDocente = new ArrayList<ExperienciaDocente>();
	private List<ExperienciaProfesional> experienciaProfesional = new ArrayList<ExperienciaProfesional>();
	private List<ProductividadIntelectual> productividadIntelectual = new ArrayList<ProductividadIntelectual>();
	
	public String generarHv(Inscripcion inscripcion) throws Exception
	{
		int registros = 0; 
		this.aspirante = inscripcion.getAspirante();
		this.infoPersonal = servicio.buscarInfoPersonal(aspirante);
		this.formAcademica = servicio.buscarFormAcademica(aspirante);
		this.segundaLengua = servicio.buscarIdiomas(aspirante);
		this.experienciaDocente = servicio.buscarExperienciaDocente(aspirante);
		this.experienciaProfesional = servicio.buscarExperienciaProfesional(aspirante);
		this.productividadIntelectual = servicio.buscarProductividadIntelectual(aspirante);
		FacesContext facesContext= FacesContext.getCurrentInstance();
	    HttpServletResponse response=(HttpServletResponse)facesContext.getExternalContext().getResponse();
	    facesContext.responseComplete(); 
	    
	    Document document = new Document(PageSize.LETTER);
	    try
	    {
	    	response.setContentType("application/pdf");
	    	PdfWriter.getInstance(document, response.getOutputStream());
	    	document.open();
	    	Font bold= new Font();
	    	bold.setSize(12);
	    	bold.setStyle(1);
	    	Font fuente= new Font();
	    	fuente.setSize(12);
	    	Font tfuente= new Font();
	    	tfuente.setSize(10);
	    	Font tbold= new Font();
	    	tbold.setSize(10);
	    	tbold.setStyle(1);
	    	
	    	//ENCABEZADO
	        Paragraph titulo = new Paragraph("UNIVERSIDAD DE LOS LLANOS", bold);
	        titulo.setAlignment(1);
	        document.add(titulo);
	        
	        Paragraph titulo2 = new Paragraph("Vicerrectoría Académica \n FORMATO DE HOJA DE VIDA PARA PARTICIPAR", fuente);
	        titulo2.setAlignment(1);
	        document.add(titulo2);
	        
	        Paragraph titulo4 = new Paragraph("CONCURSO DE MÉRITOS PROFESORES DE PLANTA "+inscripcion.getConcurso().getConvocatoria().getIdConvocatoria(), bold);
	        titulo4.setAlignment(1);
	        document.add(titulo4);
	        document.add(new Paragraph(" "));
	        document.add(new Paragraph("DATOS DEL CONCURSO:", bold));
	        document.add(new Paragraph(" "));
	        
	        //TABLA DATOS DEL CONCURSO
	        Paragraph concurso = new Paragraph("No. CONCURSO", tbold);
	        Paragraph unidad = new Paragraph("UNIDAD ACADÉMICA", tbold);
	        Paragraph facultad = new Paragraph("FACULTAD", tbold);
	        PdfPTable tabla1 = new PdfPTable(3);
	        float[] medidaCeldas = {1.55f, 1.85f, 1.85f};
	        tabla1.setWidths(medidaCeldas);
	        PdfPCell celda =new PdfPCell (concurso);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla1.addCell(celda);
	        celda = new PdfPCell (unidad);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla1.addCell(celda);
	        celda = new PdfPCell (facultad);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla1.addCell(celda);
	        celda = new PdfPCell (new Paragraph(inscripcion.getConcurso().getIdConcurso(), tfuente));
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla1.addCell(celda);
	        celda = new PdfPCell (new Paragraph(inscripcion.getConcurso().getUnidadAcademica().getNombre(), tfuente));
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla1.addCell(celda);
	        celda = new PdfPCell (new Paragraph(inscripcion.getConcurso().getUnidadAcademica().getFacultad().getNombre(), tfuente));
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla1.addCell(celda);
	        document.add(tabla1);
	        
	        //DATOS PERSNALES
	        document.add(new Paragraph(" "));
	        document.add(new Paragraph("DATOS PERSONALES:", bold));     
	        Chunk nombre1 = new Chunk("   Nombre:  ",bold);
	        Chunk nombre2 = new Chunk(aspirante.getNombre()+" "+aspirante.getApellido1()+" "+aspirante.getApellido2(),fuente);
	        Chunk tipoId1 = new Chunk("\n   Tipo identificación:  ",bold);
	        Chunk tipoId2 = new Chunk(aspirante.getTipoId()+"    ",fuente);
	        Chunk No1 = new Chunk("No.  ",bold);
	        Chunk No2 = new Chunk(aspirante.getIdAspirante()+"    ",fuente);
	        Chunk nacionalidad1 = new Chunk("Nacionalidad:  ",bold);
	        Chunk nacionalidad2 = new Chunk(infoPersonal.getNacionalidad(),fuente);
	        Chunk telefono1 = new Chunk("\n   Telefono:  ",bold);
	        Chunk telefono2 = new Chunk(infoPersonal.getTelefono()+"    ",fuente);
	        Chunk acel1 = new Chunk("Celular1:  ",bold);
	        Chunk acel2 = new Chunk(infoPersonal.getCelular1()+"    ",fuente);
	        Chunk bcel1 = new Chunk("Celular2:  ",tbold);
	        Chunk bcel2 = new Chunk(infoPersonal.getCelular2()+"    ",fuente);
	        Chunk email1 = new Chunk("\n   Email:  ",bold);
	        Chunk email2 = new Chunk(aspirante.getEmail()+"    ",fuente);
	        Chunk idioma1 = new Chunk("Idioma nativo:  ",bold);
	        Chunk idioma2 = new Chunk(infoPersonal.getIdioma()+"    ",fuente);
	        Phrase p = new Phrase();
	        p.add(nombre1);p.add(nombre2);
	        p.add(tipoId1);p.add(tipoId2);
	        p.add(No1);p.add(No2);
	        p.add(nacionalidad1);p.add(nacionalidad2);
	        p.add(telefono1);p.add(telefono2);
	        p.add(acel1);p.add(acel2);
	        p.add(bcel1);p.add(bcel2);
	        p.add(email1);p.add(email2);
	        p.add(idioma1);p.add(idioma2);
	        document.add(p);
	        
	        document.add(new Paragraph("FECHA Y LUGAR DE NACIMIENTO:", bold));    
	        Chunk fecha1 = new Chunk("   Fecha:  ",bold);
	        Chunk fecha2 = new Chunk(infoPersonal.getFechaNacimiento()+"",fuente);
	        Chunk nMun1 = new Chunk("\n   Municipio:  ",bold);
	        Chunk nMun2 = new Chunk(infoPersonal.getMunNacimiento()+"    ",fuente);
	        Chunk nDep1 = new Chunk("Departamento:  ",bold);
	        Chunk nDep2 = new Chunk(infoPersonal.getDepNacimiento()+"    ",fuente);
	        Chunk nPais1 = new Chunk("Pais:  ",bold);
	        Chunk nPais2 = new Chunk(infoPersonal.getPaisNacimiento()+"    ",fuente);
	        p = new Phrase();
	        p.add(fecha1);p.add(fecha2);
	        p.add(nMun1);p.add(nMun2);
	        p.add(nDep1);p.add(nDep2);
	        p.add(nPais1);p.add(nPais2);
	        document.add(p);
	        
	        document.add(new Paragraph("DIRECCIÓN DE CORRESPONDENCIA:", bold));    
	        Chunk cMun1 = new Chunk("   Municipio:  ",bold);
	        Chunk cMun2 = new Chunk(infoPersonal.getMunCorrespondencia()+"    ",fuente);
	        Chunk cDep1 = new Chunk("Departamento:  ",bold);
	        Chunk cDep2 = new Chunk(infoPersonal.getDepCorrespondencia()+"    ",fuente);
	        Chunk cPais1 = new Chunk("Pais:  ",bold);
	        Chunk cPais2 = new Chunk(infoPersonal.getPaisCorrespondencia()+"    ",fuente);
	        Chunk dir1 = new Chunk("\n   Dirección:  ",bold);
	        Chunk dir2 = new Chunk(infoPersonal.getDireccion()+"    ",fuente);
	        Chunk barrio1 = new Chunk("Barrio:  ",bold);
	        Chunk barrio2 = new Chunk(infoPersonal.getBarrio()+"    ",fuente);
	        p = new Phrase();
	        p.add(cMun1);p.add(cMun2);
	        p.add(cDep1);p.add(cDep2);
	        p.add(cPais1);p.add(cPais2);
	        p.add(dir1);p.add(dir2);
	        p.add(barrio1);p.add(barrio2);
	        document.add(p);
	        
	        //FORMACIÓN ACADEMICA - Tecnológica y Pregrado
	        document.add(new Paragraph(" "));
	        document.add(new Paragraph("FORMACIÓN ACADÉMICA", bold));
	        document.add(new Paragraph("   TECNOLÓGICO Y PREGRADO:", bold));
	        document.add(new Paragraph(" "));
	        
	        Paragraph modalidad = new Paragraph("MODALIDAD", tbold);
	        Paragraph tituloO = new Paragraph("TÍTULO OBTENIDO", tbold);
	        Paragraph fecha = new Paragraph("FECHA GRADO", tbold);
	        Paragraph tar = new Paragraph("No.TARJETA", tbold);
	        PdfPTable tabla2 = new PdfPTable(4);
	        tabla2.setWidthPercentage(new float[] { 120, 315,80,100},PageSize.A4);
	        celda =new PdfPCell (modalidad);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla2.addCell(celda);
	        celda = new PdfPCell (tituloO);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla2.addCell(celda);
	        celda = new PdfPCell (fecha);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla2.addCell(celda);
	        celda = new PdfPCell (tar);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla2.addCell(celda);
	        
	        for(int i = 0; i<formAcademica.size(); i++)
	        {
	        	if(formAcademica.get(i).getModalidad().equals("TECNOLÓGICA")||formAcademica.get(i).getModalidad().equals("UNIVERSITARIA"))
	        	{
	        		registros ++;
		        	celda = new PdfPCell (new Paragraph(formAcademica.get(i).getModalidad(), tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        tabla2.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(formAcademica.get(i).getTitulo(), tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla2.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(formAcademica.get(i).getFechaGrado()+"", tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla2.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(formAcademica.get(i).getNumeroTarjeta(), tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla2.addCell(celda);
	        	}
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(5);
	 	        tabla2.addCell(celda);
        	}
	        document.add(tabla2);
	        
	        //FORMACIÓN ACADEMICA - Posgrado
	        document.add(new Paragraph(" "));
	        document.add(new Paragraph("   POSGRADO:", bold));
	        document.add(new Paragraph(" "));
	        
	        Paragraph modalidad2 = new Paragraph("MODALIDAD", tbold);
	        Paragraph tituloO2 = new Paragraph("TÍTULO QUE OTORGA EL PROGRAMA", tbold);
	        Paragraph nSem = new Paragraph("SEMESTRES APROBRADOS", tbold);
	        Paragraph graduado = new Paragraph("GRADUADO", tbold);
	        Paragraph fechaG = new Paragraph("FECHA GRADO", tbold);
	        registros = 0;
	        PdfPTable tabla3 = new PdfPTable(5);
	        tabla3.setWidthPercentage(new float[] { 120, 250,90,75,80},PageSize.A4);
	        celda =new PdfPCell (modalidad2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla3.addCell(celda);
	        celda = new PdfPCell (tituloO2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla3.addCell(celda);
	        celda = new PdfPCell (nSem);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla3.addCell(celda);
	        celda = new PdfPCell (graduado);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla3.addCell(celda);
	        celda = new PdfPCell (fechaG);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla3.addCell(celda);
	       
	        for(int i = 0; i<formAcademica.size(); i++)
	        {
	        	if(formAcademica.get(i).getModalidad().equals("ESPECIALIZACIÓN")||formAcademica.get(i).getModalidad().equals("MAESTRÍA")||formAcademica.get(i).getModalidad().equals("DOCTORADO"))
	        	{   
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(formAcademica.get(i).getModalidad(), tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        tabla3.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(formAcademica.get(i).getTitulo(), tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla3.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(formAcademica.get(i).getSemestresAprobados(), tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla3.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(formAcademica.get(i).getGraduado(), tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla3.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(formAcademica.get(i).getFechaGrado()+"", tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla3.addCell(celda);
	        	}
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(5);
	 	        tabla3.addCell(celda);
        	}
	        document.add(tabla3);
	        
	        
	        //NNUEVA PAGINA
	        document.newPage();
	        document.add(titulo);
	        document.add(titulo2);
	        document.add(titulo4);
	        
	        //IDIOMAS
	        document.add(new Paragraph(" "));
	        document.add(new Paragraph("IDIOMAS:", bold));
	        document.add(new Paragraph(" "));
	        
	        Paragraph idioma = new Paragraph("IDIOMA", tbold);
	        Paragraph lohabla = new Paragraph("LO HABLA", tbold);
	        Paragraph lolee = new Paragraph("LO LEE", tbold);
	        Paragraph loescribe = new Paragraph("LO ESCRIBE", tbold);
	        registros = 0;
	        PdfPTable tabla4 = new PdfPTable(4);
	        tabla4.setWidthPercentage(new float[] {150, 100,100,100},PageSize.A4);
	        celda =new PdfPCell (idioma);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla4.addCell(celda);
	        celda = new PdfPCell (lohabla);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla4.addCell(celda);
	        celda = new PdfPCell (lolee);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla4.addCell(celda);
	        celda = new PdfPCell (loescribe);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla4.addCell(celda);
	        
	        for(int i = 0; i<segundaLengua.size(); i++)
	        {
	        	registros++;
	        	celda = new PdfPCell (new Paragraph(segundaLengua.get(i).getNombre(), tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setPadding(4);
	 	        tabla4.addCell(celda);
	 	       celda = new PdfPCell (new Paragraph(segundaLengua.get(i).getHabla(), tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setPadding(4);
	 	        tabla4.addCell(celda);
	 	       celda = new PdfPCell (new Paragraph(segundaLengua.get(i).getLee(), tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setPadding(4);
	 	        tabla4.addCell(celda);
	 	       celda = new PdfPCell (new Paragraph(segundaLengua.get(i).getEscribe(), tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setPadding(4);
	 	        tabla4.addCell(celda);
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(4);
	 	        tabla4.addCell(celda);
        	}
	        document.add(tabla4);
	        
	        //EXPERIENCIA DOCENTE - Modalidad Tiempo Completo
	        document.add(new Paragraph(" "));
	        document.add(new Paragraph("EXPERIENCIA DOCENTE", bold));
	        document.add(new Paragraph("   MODALIDAD TIEMPO COMPLETO:", bold));
	        document.add(new Paragraph(" "));
	        
	        Paragraph institucion = new Paragraph("INSTITUCIÓN", tbold);
	        Paragraph finicio = new Paragraph("FECHA INICIO", tbold);
	        Paragraph fretiro = new Paragraph("FECHA RETIRO", tbold);
	        Paragraph folio = new Paragraph("FOLIO", tbold);
	        registros = 0;
	        PdfPTable tabla5 = new PdfPTable(4);
	        tabla5.setWidthPercentage(new float[] {330, 100,100,70},PageSize.A4);
	        celda =new PdfPCell (institucion);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla5.addCell(celda);
	        celda = new PdfPCell (finicio);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla5.addCell(celda);
	        celda = new PdfPCell (fretiro);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla5.addCell(celda);
	        celda = new PdfPCell (folio);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla5.addCell(celda);
	        
	       
	        
	        for(int i = 0; i<experienciaDocente.size(); i++)
	        {
	        	if(experienciaDocente.get(i).getDedicacion().equals("TC"))
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getInstitucion(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        tabla5.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getFechaVinculacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla5.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getFechaRetiro()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla5.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getFolio()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla5.addCell(celda);
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(4);
	 	        tabla5.addCell(celda);
        	} 
	        document.add(tabla5);
	        
	        //EXPERIENCIA DOCENTE - Modalidad Medio Tiempo
	        document.add(new Paragraph("   MODALIDAD MEDIO TIEMPO:", bold));
	        document.add(new Paragraph(" "));
	        registros = 0;
	        PdfPTable tabla6 = new PdfPTable(4);
	        tabla6.setWidthPercentage(new float[] {330, 100,100,70},PageSize.A4);
	        celda =new PdfPCell (institucion);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla6.addCell(celda);
	        celda = new PdfPCell (finicio);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla6.addCell(celda);
	        celda = new PdfPCell (fretiro);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla6.addCell(celda);
	        celda = new PdfPCell (folio);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla6.addCell(celda);
	        
	       
	        
	        for(int i = 0; i<experienciaDocente.size(); i++)
	        {
	        	if(experienciaDocente.get(i).getDedicacion().equals("MT"))
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getInstitucion(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        tabla6.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getFechaVinculacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla6.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getFechaRetiro()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla6.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getFolio()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla6.addCell(celda);
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(4);
	 	        tabla6.addCell(celda);
        	} 
	        document.add(tabla6);
	        
	      //EXPERIENCIA DOCENTE - Modalidad Catedra
	        document.add(new Paragraph("   MODALIDAD CÁTEDRA:", bold));
	        document.add(new Paragraph(" "));
	        Paragraph institucion2 = new Paragraph("INSTITUCIÓN", tbold);
	        Paragraph horasSemana = new Paragraph("HRS/SEMANA", tbold);
	        Paragraph semanasSemestre = new Paragraph("SEMANAS/SEMESTRE", tbold);
	        Paragraph horasSemestre = new Paragraph("HORAS/SEMESTRE", tbold);
	        Paragraph finicio2 = new Paragraph("FECHA INICIO", tbold);
	        Paragraph fretiro2 = new Paragraph("FECHA RETIRO", tbold);
	        Paragraph folio2 = new Paragraph("FOLIO", tbold);
	        registros = 0;
	        
	        PdfPTable tabla7 = new PdfPTable(7);
	        tabla7.setWidthPercentage(new float[] {180,70,70,70,80,80,50},PageSize.A4);
	        celda =new PdfPCell (institucion2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla7.addCell(celda);
	        celda = new PdfPCell (horasSemana);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla7.addCell(celda);
	        celda = new PdfPCell (semanasSemestre);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla7.addCell(celda);
	        celda = new PdfPCell (horasSemestre);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla7.addCell(celda);
	        celda = new PdfPCell (finicio2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla7.addCell(celda);
	        celda = new PdfPCell (fretiro2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla7.addCell(celda);
	        celda = new PdfPCell (folio2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla7.addCell(celda);
	        
	        for(int i = 0; i<experienciaDocente.size(); i++)
	        {
	        	if(experienciaDocente.get(i).getDedicacion().equals("MC"))
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getInstitucion(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        tabla7.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getHorasSemana()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla7.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getSemanasSemestre()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla7.addCell(celda);
		 	        
		 	       celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getSemanasSemestre()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla7.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getFechaVinculacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla7.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getFechaRetiro()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla7.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaDocente.get(i).getFolio()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla7.addCell(celda);
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(7);
	 	        tabla7.addCell(celda);
        	} 
	        document.add(tabla7);
	        
	        //NNUEVA PAGINA
	        document.newPage();
	        document.add(titulo);
	        document.add(titulo2);
	        document.add(titulo4);
	        
	        
	        //EXPERIENCIA PROFESIONAL
	        document.add(new Paragraph(" "));
	        document.add(new Paragraph("EXPERIENCIA PROFESIONAL:", bold));
	        document.add(new Paragraph(" "));
	        
	        Paragraph tipoCertificacion = new Paragraph("TIPO CERTIFICACION", tbold);
	        Paragraph empresa = new Paragraph("EMPRESA O INSTITUCIÓN", tbold);
	        Paragraph cargo = new Paragraph("CARGO O FUNCIÓN", tbold);
	        Paragraph finicio3 = new Paragraph("FECHA INICIO", tbold);
	        Paragraph fretiro3 = new Paragraph("FECHA RETIRO", tbold);
	        Paragraph folio3 = new Paragraph("FOLIO", tbold);
	        registros = 0;
	        
	        PdfPTable tabla8 = new PdfPTable(6);
	        tabla8.setWidthPercentage(new float[] {180,105,105,80,80,50},PageSize.A4);
	        celda =new PdfPCell (tipoCertificacion);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla8.addCell(celda);
	        celda = new PdfPCell (empresa);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla8.addCell(celda);
	        celda = new PdfPCell (cargo);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla8.addCell(celda);
	        celda = new PdfPCell (finicio3);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla8.addCell(celda);
	        celda = new PdfPCell (fretiro3);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla8.addCell(celda);
	        celda = new PdfPCell (folio3);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla8.addCell(celda);
	        
	        for(int i = 0; i<experienciaProfesional.size(); i++)
	        {
	        
	        	if(experienciaProfesional.get(i).getTipo().equals("PROFESIONAL"))
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(experienciaProfesional.get(i).getTipoVinculacion(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        
		 	        tabla8.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaProfesional.get(i).getEmpresa()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla8.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaProfesional.get(i).getCargo()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla8.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaProfesional.get(i).getFechaVinculacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla8.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaProfesional.get(i).getFechaRetiro()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla8.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaProfesional.get(i).getFolio()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla8.addCell(celda);
		 	      
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(6);
	 	        tabla8.addCell(celda);
        	} 
	        document.add(tabla8);
	        
	      //EXPERIENCIA INVESTIGATIVA
	        document.add(new Paragraph(" "));
	        document.add(new Paragraph("EXPERIENCIA INVESTIGATIVA EN INSTITUCIONES NO DEDICADAS A LA DOCENCIA:", bold));
	        document.add(new Paragraph(" "));
	        PdfPTable tabla9 = new PdfPTable(6);
	        registros = 0;
	        tabla9.setWidthPercentage(new float[] {180,105,105,80,80,50},PageSize.A4);
	        celda =new PdfPCell (tipoCertificacion);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla9.addCell(celda);
	        celda = new PdfPCell (empresa);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla9.addCell(celda);
	        celda = new PdfPCell (cargo);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla9.addCell(celda);
	        celda = new PdfPCell (finicio3);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla9.addCell(celda);
	        celda = new PdfPCell (fretiro3);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla9.addCell(celda);
	        celda = new PdfPCell (folio3);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla9.addCell(celda);
	        
	        
	        for(int i = 0; i<experienciaProfesional.size(); i++)
	        {
	        
	        	if(experienciaProfesional.get(i).getTipo().equals("INVESTIGATIVA"))
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(experienciaProfesional.get(i).getTipoVinculacion(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        
		 	        tabla9.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaProfesional.get(i).getEmpresa()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla9.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaProfesional.get(i).getCargo()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla9.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaProfesional.get(i).getFechaVinculacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla9.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(experienciaProfesional.get(i).getFechaRetiro()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla9.addCell(celda);
		 	        
		 	       celda = new PdfPCell (new Paragraph(experienciaProfesional.get(i).getFolio()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla9.addCell(celda);
		 	      
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(6);
	 	        tabla9.addCell(celda);
        	} 
	        document.add(tabla9);
	        
	        //NNUEVA PAGINA
	        document.newPage();
	        document.add(titulo);
	        document.add(titulo2);
	        document.add(titulo4);
	        
	        
	        //PRODUCTIVIDAD INTELECTUAL - Tipo1
	        document.add(new Paragraph(" "));
	        document.add(new Paragraph("PRODUCTIVIDAD INTELECTUAL", bold));
	        document.add(new Paragraph("   1. ARTICULOS REVISTAS INDEXADAS U HOMOLOGADAS:", bold));
	        document.add(new Paragraph(" "));
	        
	        Paragraph titulo3 = new Paragraph("TÍTULO DEL ARTICULO", tbold);
	        Paragraph revista = new Paragraph("REVISTA", tbold);
	        Paragraph categoria = new Paragraph("CAT", tbold);
	        Paragraph volumen = new Paragraph("VOL", tbold);
	        Paragraph numero = new Paragraph("NUM", tbold);
	        Paragraph anio = new Paragraph("AÑO", tbold);
	        Paragraph autores = new Paragraph("AUT", tbold);
	        Paragraph issn = new Paragraph("ISSN", tbold);
	        Paragraph folios = new Paragraph("FOLIOS", tbold);
	        
	        registros = 0;
	        
	        PdfPTable tabla10 = new PdfPTable(9);
	        tabla10.setWidthPercentage(new float[] {160,80,50,50,50,50,50,60,50},PageSize.A4);
	        celda =new PdfPCell (titulo3);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla10.addCell(celda);
	        celda = new PdfPCell (revista);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla10.addCell(celda);
	        celda = new PdfPCell (categoria);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla10.addCell(celda);
	        celda = new PdfPCell (volumen);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla10.addCell(celda);
	        celda = new PdfPCell (numero);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla10.addCell(celda);
	        celda = new PdfPCell (anio);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla10.addCell(celda);
	        celda = new PdfPCell (autores);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla10.addCell(celda);
	        celda = new PdfPCell (issn);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla10.addCell(celda);
	        celda = new PdfPCell (folios);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla10.addCell(celda);
	        
	        for(int i = 0; i<productividadIntelectual.size(); i++)
	        {
	        
	        	if(productividadIntelectual.get(i).getTipo()==1)
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getNombre(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        
		 	        tabla10.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getRevista(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla10.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getCategoria()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla10.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getVolumen()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla10.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getNumero()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla10.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getAnio()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla10.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getAutores()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla10.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getIssn()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla10.addCell(celda);
		 	        
		 	       celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFolio()+" - "+productividadIntelectual.get(i).getFolio2(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla10.addCell(celda);
		 	      
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(9);
	 	        tabla10.addCell(celda);
        	} 
	        document.add(tabla10);
	 
	      //PRODUCTIVIDAD INTELECTUAL - Tipo2
	        document.add(new Paragraph("   2. LIBROS PRODUCTO DE INVESTIGACIÓN:", bold));
	        document.add(new Paragraph(" "));
	        
	        Paragraph titulo5 = new Paragraph("TÍTULO DEL LIBRO", tbold);
	        Paragraph isbn = new Paragraph("ISBN", tbold);
	        Paragraph autores2 = new Paragraph("AUTORES", tbold);
	        Paragraph fechaPublicacion = new Paragraph("FECHA PUBLICACION", tbold);
	        Paragraph editorial = new Paragraph("EDITORIAL", tbold);
	        registros = 0;
	        PdfPTable tabla11 = new PdfPTable(5);
	        tabla11.setWidthPercentage(new float[] {260,80,70,90,100},PageSize.A4);
	        celda =new PdfPCell (titulo5);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla11.addCell(celda);
	        celda = new PdfPCell (isbn);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla11.addCell(celda);
	        celda = new PdfPCell (autores2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla11.addCell(celda);
	        celda = new PdfPCell (fechaPublicacion);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla11.addCell(celda);
	        celda = new PdfPCell (editorial);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla11.addCell(celda);
	        
	        
	        for(int i = 0; i<productividadIntelectual.size(); i++)
	        {
	        
	        	if(productividadIntelectual.get(i).getTipo()==2)
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getNombre(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        
		 	        tabla11.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getIsbn()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla11.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getAutores()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla11.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFechaPublicacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla11.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getEditorial()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla11.addCell(celda); 
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(5);
	 	        tabla11.addCell(celda);
        	} 
	        document.add(tabla11);
	        
	        //PRODUCTIVIDAD INTELECTUAL - Tipo3
	        document.add(new Paragraph("   3. LIBROS DE TEXTO:", bold));
	        document.add(new Paragraph(" "));
	        registros = 0;
	        PdfPTable tabla12 = new PdfPTable(5);
	        tabla12.setWidthPercentage(new float[] {260,80,70,90,100},PageSize.A4);
	        celda =new PdfPCell (titulo5);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla12.addCell(celda);
	        celda = new PdfPCell (isbn);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla12.addCell(celda);
	        celda = new PdfPCell (autores2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla12.addCell(celda);
	        celda = new PdfPCell (fechaPublicacion);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla12.addCell(celda);
	        celda = new PdfPCell (editorial);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla12.addCell(celda);
	        
	        
	        for(int i = 0; i<productividadIntelectual.size(); i++)
	        {
	        
	        	if(productividadIntelectual.get(i).getTipo()==3)
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getNombre(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        
		 	        tabla12.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getIsbn()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla12.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getAutores()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla12.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFechaPublicacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla12.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getEditorial()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla12.addCell(celda); 
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(5);
	 	        tabla12.addCell(celda);
        	} 
	        document.add(tabla12);
	        
	        //PRODUCTIVIDAD INTELECTUAL - Tipo4
	        document.add(new Paragraph("   4. TRADUCCIONES DE LIBRO:", bold));
	        document.add(new Paragraph(" "));
	        Paragraph titulo6 = new Paragraph("TÍTULO LIBRO (ORIGNAL)", tbold);
	        Paragraph autores3 = new Paragraph("AUT", tbold);
	        Paragraph fechaPublicacion2 = new Paragraph("FECHA PUBLICACI", tbold);
	        Paragraph editorial2 = new Paragraph("EDITORIAL", tbold);
	        Paragraph tituloT = new Paragraph("TÍTULO LIBRO (TRADUCCIÓN)", tbold);
	        Paragraph autores3T = new Paragraph("AUT", tbold);
	        Paragraph fechaPublicacion2T = new Paragraph("FECHA PUBLICACI", tbold);
	        Paragraph editorial2T = new Paragraph("EDITORIAL", tbold);
	        Paragraph folioT = new Paragraph("FOLIO AUTORI", tbold);
	        registros = 0;
	        PdfPTable tabla13 = new PdfPTable(9);
	        tabla13.setWidthPercentage(new float[] {115,30,70,65,115,30,70,65,50},PageSize.A4);
	        celda =new PdfPCell (titulo6);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla13.addCell(celda);
	        celda = new PdfPCell (autores3);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla13.addCell(celda);
	        celda = new PdfPCell (fechaPublicacion2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla13.addCell(celda);
	        celda = new PdfPCell (editorial2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla13.addCell(celda);
	        celda = new PdfPCell (tituloT);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla13.addCell(celda);
	        celda = new PdfPCell (autores3T);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla13.addCell(celda);
	        celda = new PdfPCell (fechaPublicacion2T);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla13.addCell(celda);
	        celda = new PdfPCell (editorial2T);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla13.addCell(celda);
	        celda = new PdfPCell (folioT);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla13.addCell(celda);
	        
	        
	        for(int i = 0; i<productividadIntelectual.size(); i++)
	        {
	        
	        	if(productividadIntelectual.get(i).getTipo()==4)
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getNombre(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        
		 	        tabla13.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getAutores()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla13.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFechaPublicacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla13.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getEditorial()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla13.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getNombreTraduccion(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        tabla13.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getAutoresTraduccion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla13.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFechaPublicacionTraducccion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla13.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getEditorialTraduccion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla13.addCell(celda);
		 	        
		 	       celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFolio()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla13.addCell(celda);
		 	        
		 	        
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(9);
	 	        tabla13.addCell(celda);
        	} 
	        document.add(tabla13);
	        
	      //PRODUCTIVIDAD INTELECTUAL - Tipo5
	        document.add(new Paragraph("   5. PROYECTOS DE INVESTIGACIÓN:", bold));
	        document.add(new Paragraph(" "));
	        Paragraph titulo7 = new Paragraph("NOMBRE DEL PROYECTO", tbold);
	        Paragraph entidad = new Paragraph("ENTIDAD FINANCIADORA", tbold);
	        Paragraph fechaInico = new Paragraph("FECHA INICIO", tbold);
	        Paragraph fechaTerminacion = new Paragraph("FECHA TERMINACIÓN", tbold);
	        Paragraph folio4 = new Paragraph("FOLIO", tbold);
	        registros = 0;
	        PdfPTable tabla14 = new PdfPTable(5);
	        tabla14.setWidthPercentage(new float[] {280,110,80,85,50},PageSize.A4);
	        celda =new PdfPCell (titulo7);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla14.addCell(celda);
	        celda = new PdfPCell (entidad);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla14.addCell(celda);
	        celda = new PdfPCell (fechaInico);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla14.addCell(celda);
	        celda = new PdfPCell (fechaTerminacion);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla14.addCell(celda);
	        celda = new PdfPCell (folio4);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla14.addCell(celda);
	        
	        
	        
	        for(int i = 0; i<productividadIntelectual.size(); i++)
	        {
	        
	        	if(productividadIntelectual.get(i).getTipo()==5)
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getNombre(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        
		 	        tabla14.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getInstitucion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla14.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFechaPublicacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla14.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFechaTerminacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla14.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFolio()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        tabla14.addCell(celda); 
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(5);
	 	        tabla14.addCell(celda);
        	} 
	        document.add(tabla14);
	        
	        //PRODUCTIVIDAD INTELECTUAL - Tipo6
	        document.add(new Paragraph("   6. DOCUMENTOS DE CONDICIONES MINIMAS PRESENTADOS ANTE EL MEN:", bold));
	        document.add(new Paragraph(" "));
	        Paragraph titulo8 = new Paragraph("NOMBRE DEL DOCUMENTO", tbold);
	        Paragraph institucion3 = new Paragraph("INSTITUCIÓN", tbold);
	        Paragraph fechaTerminacion2 = new Paragraph("FECHA TERMINACIÓN", tbold);
	        Paragraph folio5 = new Paragraph("FOLIO", tbold);
	        registros = 0;
	        PdfPTable tabla15 = new PdfPTable(4);
	        tabla15.setWidthPercentage(new float[] {310,130,90,80},PageSize.A4);
	        celda =new PdfPCell (titulo8);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla15.addCell(celda);
	        celda = new PdfPCell (institucion3);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla15.addCell(celda);
	        celda = new PdfPCell (fechaTerminacion2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla15.addCell(celda);
	        celda = new PdfPCell (folio5);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla15.addCell(celda); 
	        
	        for(int i = 0; i<productividadIntelectual.size(); i++)
	        {
	        
	        	if(productividadIntelectual.get(i).getTipo()==6)
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getNombre(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        
		 	        tabla15.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getInstitucion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla15.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFechaTerminacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla15.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFolio()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla15.addCell(celda);
		 	        
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(4);
	 	        tabla15.addCell(celda);
        	} 
	        document.add(tabla15);
	        
	        //PRODUCTIVIDAD INTELECTUAL - Tipo7
	        document.add(new Paragraph("   7. DOCUMENTOS INSTITUCIONALES DE ACREDITACIÓN DE ALTA CALIDAD:", bold));
	        document.add(new Paragraph(" "));
	        registros = 0;
	        PdfPTable tabla16 = new PdfPTable(4);
	        tabla16.setWidthPercentage(new float[] {310,130,90,80},PageSize.A4);
	        celda =new PdfPCell (titulo8);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla16.addCell(celda);
	        celda = new PdfPCell (institucion3);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla16.addCell(celda);
	        celda = new PdfPCell (fechaTerminacion2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla16.addCell(celda);
	        celda = new PdfPCell (folio5);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla16.addCell(celda); 
	        
	        for(int i = 0; i<productividadIntelectual.size(); i++)
	        {
	        
	        	if(productividadIntelectual.get(i).getTipo()==7)
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getNombre(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        
		 	        tabla16.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getInstitucion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla16.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFechaTerminacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla16.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFolio()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla16.addCell(celda);
		 	        
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(4);
	 	        tabla16.addCell(celda);
        	} 
	        document.add(tabla16);
	        
	      //PRODUCTIVIDAD INTELECTUAL - Tipo8
	        document.add(new Paragraph("   8. ARTICULOS EN REVISTAS ACADÉMICAS O DE PROYECCIÓN SOCIAL NO INDEXADAS:", bold));
	        document.add(new Paragraph(" "));
	        Paragraph titulo9 = new Paragraph("TÍTULO DEL ARTICULO", tbold);
	        Paragraph revista2 = new Paragraph("REVISTA", tbold);
	        Paragraph autores4 = new Paragraph("AUTORES", tbold);
	        Paragraph fecha3 = new Paragraph("FECHA DE PUBLICACIÓN", tbold);
	        Paragraph folio6 = new Paragraph("FOLIO", tbold);
	        
	        
	        registros = 0;
	        PdfPTable tabla17 = new PdfPTable(5);
	        tabla17.setWidthPercentage(new float[] {250,130,90,90,50},PageSize.A4);
	        celda =new PdfPCell (titulo9);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla17.addCell(celda);
	        celda = new PdfPCell (revista2);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla17.addCell(celda);
	        celda = new PdfPCell (autores4);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla17.addCell(celda);
	        celda = new PdfPCell (fecha3);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla17.addCell(celda); 
	        celda = new PdfPCell (folio6);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla17.addCell(celda); 
	        
	        for(int i = 0; i<productividadIntelectual.size(); i++)
	        {
	        
	        	if(productividadIntelectual.get(i).getTipo()==8)
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getNombre(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        
		 	        tabla17.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getRevista()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla17.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getAutores()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla17.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFechaPublicacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla17.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFolio()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla17.addCell(celda);
		 	        
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(5);
	 	        tabla17.addCell(celda);
        	} 
	        document.add(tabla17);
	        
	        //PRODUCTIVIDAD INTELECTUAL - Tipo9
	        document.add(new Paragraph("   9. REGISTROS ANTE LA DIRECCION NACIONAL DE DERECHOS DE AUTOR, DE PATENTES O DE VARIEDADES:", bold));
	        document.add(new Paragraph(" "));
	        Paragraph titulo10 = new Paragraph("NOMBRE DEL REGISTRO", tbold);
	        Paragraph numero4 = new Paragraph("NUMERO", tbold);
	        Paragraph fecha4 = new Paragraph("FECHA", tbold);
	        Paragraph entidad3 = new Paragraph("ENTIDAD QUE OTORGA", tbold);
	        Paragraph folio7 = new Paragraph("FOLIO", tbold);
	        
	        
	        registros = 0;
	        PdfPTable tabla18 = new PdfPTable(5);
	        tabla18.setWidthPercentage(new float[] {270,90,90,110,50},PageSize.A4);
	        celda =new PdfPCell (titulo10);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        celda.setPadding(6);
	        tabla18.addCell(celda);
	        celda = new PdfPCell (numero4);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla18.addCell(celda);
	        celda = new PdfPCell (fecha4);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla18.addCell(celda);
	        celda = new PdfPCell (entidad3);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla18.addCell(celda); 
	        celda = new PdfPCell (folio7);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla18.addCell(celda); 
	        
	        for(int i = 0; i<productividadIntelectual.size(); i++)
	        {
	        
	        	if(productividadIntelectual.get(i).getTipo()==9)
	        	{
	        		registros++;
		        	celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getNombre(),tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        celda.setPadding(4);
		 	        
		 	        tabla18.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getNumero()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla18.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFechaPublicacion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla18.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getInstitucion()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla18.addCell(celda);
		 	        
		 	        celda = new PdfPCell (new Paragraph(productividadIntelectual.get(i).getFolio()+"",tfuente));
		 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	        tabla18.addCell(celda);
		 	        
	        	}
	        	
	        }
	        if(registros==0)
        	{
        		celda = new PdfPCell (new Paragraph("No se encontraron registros", tfuente));
	 	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	 	        celda.setColspan(5);
	 	        tabla18.addCell(celda);
        	} 
	        document.add(tabla18);
	        
	    }catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    document.close();
	    
	    return "login";   
	}
	
	public String generarSobre(Inscripcion inscripcion) throws Exception
	{
		
		this.aspirante = inscripcion.getAspirante();
		this.infoPersonal = servicio.buscarInfoPersonal(aspirante);
		FacesContext facesContext= FacesContext.getCurrentInstance();
	    HttpServletResponse response=(HttpServletResponse)facesContext.getExternalContext().getResponse();
	    facesContext.responseComplete(); 
	    
	    Document document = new Document(PageSize.LETTER);
	    try
	    {
	    	response.setContentType("application/pdf");
	    
	    	PdfWriter.getInstance(document, response.getOutputStream());
	    	
	    	document.open();
	    	Font bold= new Font();
	    	bold.setSize(12);
	    	bold.setStyle(1);
	    	Font fuente= new Font();
	    	fuente.setSize(12);
	    	Font tfuente= new Font();
	    	tfuente.setSize(10);
	    	Font tbold= new Font();
	    	tbold.setSize(10);
	    	tbold.setStyle(1);
	    	
	    	//ENCABEZADO
	        Paragraph titulo = new Paragraph("UNIVERSIDAD DE LOS LLANOS", bold);
	        titulo.setAlignment(1);
	        document.add(titulo);
	        
	        Paragraph titulo2 = new Paragraph("Vicerrectoría Académica", fuente);
	        titulo2.setAlignment(1);
	        document.add(titulo2);
	        
	        Paragraph titulo4 = new Paragraph("PRESENTACIÓN A:",bold);
	        document.add(titulo4);
	        
	        Paragraph titulo5 = new Paragraph("INVITACIÓN PÚBLICA A CONCURSO DE MÉRITOS PARA VINCULACIÓN DE PROFESORES DE PLANTA "+inscripcion.getConcurso().getConvocatoria().getIdConvocatoria(), bold);
	        document.add(titulo5);
	        
	        document.add(new Paragraph(" "));
	        
	      //TABLA DATOS DEL CONCURSO
	        Paragraph concurso = new Paragraph("No. CONCURSO", tbold);
	        Paragraph unidad = new Paragraph("UNIDAD ACADÉMICA", tbold);
	        PdfPTable tabla1 = new PdfPTable(2);
	        float[] medidaCeldas = {1.55f, 1.85f};
	        tabla1.setWidths(medidaCeldas);
	        PdfPCell celda =new PdfPCell (concurso);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla1.addCell(celda);
	        celda = new PdfPCell (unidad);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla1.addCell(celda);
	        celda = new PdfPCell (new Paragraph(inscripcion.getConcurso().getIdConcurso(), tfuente));
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla1.addCell(celda);
	        celda = new PdfPCell (new Paragraph(inscripcion.getConcurso().getUnidadAcademica().getNombre(), tfuente));
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla1.addCell(celda);
	        document.add(tabla1);
	        
	        
	        document.add(new Paragraph(" "));
	        Chunk nombre1 = new Chunk("NOMBRE:  ",bold);
	        Chunk nombre2 = new Chunk(aspirante.getNombre()+"\n",fuente);
	        Chunk apellido1 = new Chunk("APELLIDOS:  ",bold);
	        Chunk apellido2 = new Chunk(aspirante.getApellido1()+" "+aspirante.getApellido2()+"\n",fuente);
	        Chunk id1 = new Chunk("No.IDENTIFICACIÓN:  ",bold);
	        Chunk id2 = new Chunk(aspirante.getIdAspirante()+"\n",fuente);
	        Chunk celular1 = new Chunk("CELULAR:  ",bold);
	        Chunk celular2 = new Chunk(infoPersonal.getCelular1()+"     ",fuente);
	        Chunk telefono1 = new Chunk("TELEFONO:  ",bold);
	        Chunk telefono2 = new Chunk(infoPersonal.getTelefono()+"\n",fuente);
	        Chunk direccion1 = new Chunk("DIRECCIÓN:  ",bold);
	        Chunk direccion2 = new Chunk(infoPersonal.getDireccion()+"     ",fuente);
	        Chunk barrio1 = new Chunk("BARRIO:  ",bold);
	        Chunk barrio2 = new Chunk(infoPersonal.getBarrio()+"\n",fuente);
	        Chunk mun1 = new Chunk("CIUDAD:  ",bold);
	        Chunk mun2 = new Chunk(infoPersonal.getMunCorrespondencia()+"     ",fuente);
	        Chunk dep1 = new Chunk("DEPARTAMENO:  ",bold);
	        Chunk dep2 = new Chunk(infoPersonal.getDepCorrespondencia()+"\n",fuente);
	        Chunk email1 = new Chunk("EMAIL:  ",bold);
	        Chunk email2 = new Chunk(aspirante.getEmail()+"\n",fuente);
	        
	        Phrase p = new Phrase();
	        p.add(nombre1);p.add(nombre2);
	        p.add(apellido1);p.add(apellido2);
	        p.add(id1);p.add(id2);
	        p.add(celular1);p.add(celular2);
	        p.add(telefono1);p.add(telefono2);
	        p.add(direccion1);p.add(direccion2);
	        p.add(barrio1);p.add(barrio2);
	        p.add(mun1);p.add(mun2);
	        p.add(dep1);p.add(dep2);
	        p.add(email1);p.add(email2);
	       
	        document.add(p);
	        
	        document.add(new Paragraph(" "));
	        document.add(new Paragraph(" "));
	        document.add(new Paragraph(" "));
	        Paragraph titulo6 = new Paragraph("CERTIFICO QUE ENTREGO LOS SIGUIENTES DOCUMENTOS:",bold);
	        titulo6.setAlignment(1);
	        document.add(titulo6);
	        document.add(new Paragraph(" "));
	      //FORMACIÓN ACADEMICA - Tecnológica y Pregrado
	        
	        
	        Paragraph num = new Paragraph("NO.", tbold);
	        Paragraph doc = new Paragraph("DOCUMENTOS", tbold);
	        Paragraph del = new Paragraph("DEL FOLIO", tbold);
	        Paragraph al = new Paragraph("AL FOLIO", tbold);
	        PdfPTable tabla2 = new PdfPTable(4);
	        tabla2.setWidthPercentage(new float[] { 30, 250,100,100},PageSize.A4);
	        celda =new PdfPCell (num);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla2.addCell(celda);
	        celda = new PdfPCell (doc);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla2.addCell(celda);
	        celda = new PdfPCell (del);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla2.addCell(celda);
	        celda = new PdfPCell (al);
	        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabla2.addCell(celda);
	        
	        ArrayList<String> t = new ArrayList<String>();
	        t.add("");
	        t.add("Documentación Personal(*)");
	        t.add("Formación Académica");
	        t.add("Experiencia");
	        t.add("Producción Intelectual");
	        
	        for(int i =1; i<=4;i++)
	        {
	        	celda = new PdfPCell (new Paragraph(i+"", tfuente));
		 	    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	    celda.setPadding(4);
		 	    tabla2.addCell(celda);
		 	    celda = new PdfPCell (new Paragraph(t.get(i), tfuente));
		 	    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	    tabla2.addCell(celda);
		 	    celda = new PdfPCell (new Paragraph("", tfuente));
		 	    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	    tabla2.addCell(celda);
		 	    celda = new PdfPCell (new Paragraph("", tfuente));
		 	    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		 	    tabla2.addCell(celda);
	        }	
	        document.add(tabla2);
	        
	        document.add(new Paragraph(" "));
	        Paragraph titulo7 = new Paragraph("TOTAL DE FOLIOS __________________ ",bold);
	        titulo7.setAlignment(1);
	        document.add(titulo7);
	        
	        Paragraph titulo8 = new Paragraph("*(ENUMERAR FOLIOS INCLUYENDO EL FORMATO DE HOJA DE VIDA) ", fuente);
	        titulo8.setAlignment(1);
	        document.add(titulo8);
	        Paragraph titulo9 = new Paragraph("----------------------------------------------------------------------------------------------------------------------------------------", fuente);
	        titulo9.setAlignment(1);
	        document.add(titulo9);
	        
	        Paragraph titulo10 = new Paragraph("ESPACIO RESERVADO PARA LA VICERRECTORÍA ACADÉMICA",bold);
	        titulo10.setAlignment(1);
	        document.add(titulo10);
	        document.add(new Paragraph(" "));
	        
	        Paragraph fecha = new Paragraph("FECHA", bold);
	        Paragraph hor = new Paragraph("HORA", bold);
	        Paragraph n = new Paragraph("No. RADICACIÓN", bold);
	        Paragraph q = new Paragraph("FUNCIONARIO QUIEN RECIBE", bold);
	        PdfPTable tabla3 = new PdfPTable(2);
	        tabla3.setWidthPercentage(new float[] { 250, 350},PageSize.A4);
	        celda =new PdfPCell (fecha);
	        celda.setPadding(5);
	        tabla3.addCell(celda);
	        celda = new PdfPCell (new Paragraph("", tfuente));
 	      
 	        tabla3.addCell(celda);
	        celda = new PdfPCell (hor);
	        celda.setPadding(5);
	        tabla3.addCell(celda);
	        celda = new PdfPCell (new Paragraph("", tfuente));
 	       
 	        tabla3.addCell(celda);
	        celda = new PdfPCell (n);
	        celda.setPadding(5);
	        tabla3.addCell(celda);
	        celda = new PdfPCell (new Paragraph("", tfuente));
 	      
 	        tabla3.addCell(celda);
	        celda = new PdfPCell (q);
	        celda.setPadding(5);
	        tabla3.addCell(celda);
	        
	        celda = new PdfPCell (new Paragraph("", tfuente));
	        tabla3.addCell(celda);
	        
	        document.add(tabla3);
	    }catch(Exception e)
    {
    	e.printStackTrace();
    }
    document.close();
    return "login";   
	        
	    }
	public FormaPdf(){
		super();
	}

	public Aspirante getAspirante() {
		return aspirante;
	}

	public void setAspirante(Aspirante aspirante) {
		this.aspirante = aspirante;
	}

	public InformacionPersonal getInfoPersonal() {
		return infoPersonal;
	}

	public void setInfoPersonal(InformacionPersonal infoPersonal) {
		this.infoPersonal = infoPersonal;
	}

	public List<FormacionAcademica> getFormAcademica() {
		return formAcademica;
	}

	public void setFormAcademica(List<FormacionAcademica> formAcademica) {
		this.formAcademica = formAcademica;
	}

	public List<SegundaLengua> getSegundaLengua() {
		return segundaLengua;
	}

	public void setSegundaLengua(List<SegundaLengua> segundaLengua) {
		this.segundaLengua = segundaLengua;
	}

	public List<ExperienciaDocente> getExperienciaDocente() {
		return experienciaDocente;
	}

	public void setExperienciaDocente(List<ExperienciaDocente> experienciaDocente) {
		this.experienciaDocente = experienciaDocente;
	}

	public List<ExperienciaProfesional> getExperienciaProfesional() {
		return experienciaProfesional;
	}

	public void setExperienciaProfesional(
			List<ExperienciaProfesional> experienciaProfesional) {
		this.experienciaProfesional = experienciaProfesional;
	}

	public List<ProductividadIntelectual> getProductividadIntelectual() {
		return productividadIntelectual;
	}

	public void setProductividadIntelectual(
			List<ProductividadIntelectual> productividadIntelectual) {
		this.productividadIntelectual = productividadIntelectual;
	}
	
	
	
}

package app.cond.presentacion;



import java.sql.SQLException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;
import app.cond.entidades.Convocatoria;
import app.cond.negocio.IConcursoBean;

@ManagedBean
@RequestScoped
@ViewScoped
public class FormaConvocatoria {
	
	@EJB 
	IConcursoBean servicio;
	private String idConvocatoria;
	private String tipo;
	private Integer año;
	private Integer periodo;
	private String etapa;
	private List<Convocatoria> convocatorias2;
	
	@PostConstruct 
	public void init() throws Exception
	{ 
		try 
		{ 
		this.convocatorias2 =  servicio.buscarConvocatorias();
		} catch (SQLException e) { e.printStackTrace(); } 
	}
	
	public String guardar()
	{
		try
		{
			servicio.guardarConvocatoria(idConvocatoria, tipo, año, periodo, etapa);
			this.convocatorias2 = servicio.buscarConvocatorias();
			return "configuracion";
		}catch (Exception e){
			System.out.println("ESTE ES EL ERROR: "+e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error!", e.getMessage()));
			return "el";
		}
	}
	public String crearConvocatoria() throws Exception
	{
		return "formCrearConvocatoria";
	}
	public void update(RowEditEvent event) throws Exception {
		
		Convocatoria c = (Convocatoria) event.getObject();
		servicio.editarConvocatoria(c);
    }
	public void eliminar(Convocatoria c) throws Exception 
	{
		servicio.eliminarConvocatoria(c);
		this.convocatorias2 =  servicio.buscarConvocatorias();
    }
	
	public String getIdConvocatoria() {
		return idConvocatoria;
	}
	public void setIdConvocatoria(String idConvocatoria) {
		this.idConvocatoria = idConvocatoria;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Integer getAño() {
		return año;
	}
	public void setAño(Integer año) {
		this.año = año;
	}
	public Integer getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}
	
	public List<Convocatoria> getConvocatorias2() throws Exception{
		return convocatorias2;
	}
	public void setCovocatorias2(List<Convocatoria> convocatorias){
		this.convocatorias2 = convocatorias;
	}
	public String getEtapa() {
		return etapa;
	}
	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}
    public void onRowCancel(RowEditEvent event) {
   	
    }
}

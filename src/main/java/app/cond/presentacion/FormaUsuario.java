package app.cond.presentacion;



import java.sql.SQLException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;
import app.cond.entidades.Administrador;
import app.cond.negocio.IConcursoBean;

@ManagedBean
@RequestScoped
@ViewScoped
public class FormaUsuario {
	
	@EJB 
	IConcursoBean servicio;
	
	private String usuario;
	private Integer tipo;
	private String pass;
    private String nombre;
	private String cargo;
	private String dependencia;
	private List<Administrador> usuarios;
	
	public void FormaUsurios()
	{
		
	}
	@PostConstruct 
	public void init() throws Exception
	{ 
		try 
		{ 
		this.usuarios = servicio.buscarUsuariosPorTipo(1);
		} catch (SQLException e) { e.printStackTrace(); } 
	}
	
	public String guardar()
	{
		try{
			this.tipo = 1;
			servicio.guardarUsuario(usuario, tipo, pass, nombre, cargo, dependencia);
			this.usuarios = servicio.buscarUsuariosPorTipo(1);
			return "configuracion";
		}catch (Exception e){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error!", e.getMessage()));
			return "el";
		}
	}
	public String crearUsuario() throws Exception
	{
		return "formCrearUsuario";
	}
	public void update(RowEditEvent event) throws Exception 
	{
		Administrador u = (Administrador) event.getObject();
		servicio.editaUsuario(u);
    }
	public void eliminar(Administrador u) throws Exception
	{
		servicio.eliminarUsuario(u);
		this.usuarios = servicio.buscarUsuariosPorTipo(1);
    }
    public void onRowCancel(RowEditEvent event)
    {
    	
    }

	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getDependencia() {
		return dependencia;
	}
	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}
	public List<Administrador> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(List<Administrador> usuarios) {
		this.usuarios = usuarios;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}

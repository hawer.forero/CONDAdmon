package app.cond.presentacion;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import app.cond.negocio.IConcursoBean;

@ManagedBean
@SessionScoped
public class FormaLogin 
{
	@EJB 
	IConcursoBean servicio;

	private String usuario;
	private String password;
	private Integer tiposesion;
	private String tipo;
    
	public FormaLogin()
	{
		
	}
	public String login() throws Exception
	{
		FormaLogin obj = new FormaLogin();
		String resultado = "login";
		
		if(servicio.validarUsuario(usuario)>0)
		{
			if(servicio.validarPass(usuario, password)>0)
			{
				if(servicio.validarSesion(usuario, tiposesion)>0)
				{
					HttpSession httpSession=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);   
					httpSession.setAttribute("usuario", this.usuario);
					this.tipo = ""+tiposesion;
					if(tiposesion==1)
					{
					httpSession.setAttribute("tipo", this.tipo);
					}
					resultado = "menu";
	    		}
	    		else
	    		{
	    			obj.mostrarMensajeError("tipo de sesión no corresponde");
	    			resultado = "login";
	    		}
	        }
	        else
	        {
	        	obj.mostrarMensajeError("contraseña incorrecta");
	        	resultado = "login";
	        }		
		}
		else
		{
			obj.mostrarMensajeError("usuario no existe");
			resultado = "login";
		}	
	    return resultado;   
	}
	public String cerrarSesion()
	{
		this.usuario=null;
		this.password=null;
		this.tiposesion=null;
		HttpSession httpSession=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);   
		httpSession.invalidate();
	    return "/login";
	 }
	 public void mostrarMensajeError(String msj)
	 {
		 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,msj,null));
	 }
	public Integer getTiposesion() 
	{
		return tiposesion;
	}
	public void setTiposesion(Integer tiposesion) 
	{
		this.tiposesion = tiposesion;
	}
	public String getUsuario() 
	{
		return usuario;
	}
	public void setUsuario(String usuario) 
	{
		this.usuario = usuario;
	}
	public String getPassword() 
	{
	    return password;
	}
	public void setPassword(String password) 
	{
	    this.password = password;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}

package app.cond.presentacion;


import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.cond.entidades.Aspirante;
import app.cond.entidades.Convocatoria;
import app.cond.entidades.Inscripcion;
import app.cond.negocio.IConcursoBean;















import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
 













import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

@ManagedBean
@SessionScoped
@ViewScoped
public class FormaConsultar {
	
	@EJB 
	IConcursoBean servicio;
	private String idAspirante;
	private String mostrar;
	private String mostrar2;
	private List<Inscripcion> inscripciones;
	private List<Inscripcion> inscripciones2;
	private Aspirante aspirante;
	private Convocatoria convocatoria;
	private int numero;
	


	@PostConstruct 
	public void init() throws Exception
	{ 
		this.mostrar =  mostrar2;
		this.inscripciones =inscripciones2;
		
	}


	public void buscar(String idConvocatoria) throws Exception
	{
		this.aspirante = servicio.buscarAspirante(idAspirante);
		if(aspirante != null)
    	{
			this.mostrar2 = "true";
			this.mostrar =  mostrar2;
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Nombre del Aspirante:", aspirante.getNombre()+" "+aspirante.getApellido1()+" "+aspirante.getApellido2()));
			
    	}
		else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"LO SENTIMOS", "El Aspirante no esta registrado"));
		}
		try 
		{ 
		this.convocatoria = servicio.buscarConvocatoria(idConvocatoria);
		this.inscripciones2 = servicio.buscarInscripcionePorAspirante(convocatoria, aspirante);
		this.inscripciones =inscripciones2;
		}
		catch (SQLException e) 
		{
			e.printStackTrace(); 
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"LO SENTIMOS", e.getMessage()));
			this.mostrar2 =  "false";
			this.mostrar =  mostrar2;
		} 
	}
	
	public void generarHvlocal(Inscripcion inscripcion){
		try {
			 System.out.println("Entro en la funcion PDF");
            OutputStream file = new FileOutputStream(new File("C:\\Users\\Desarrollo\\Test.pdf"));
           
 
            Document document = new Document();
            PdfWriter.getInstance(document, file);
 
            document.open();
            document.add(new Paragraph("Hello World, iText"+inscripcion.getAspirante().getNombre()));
            document.add(new Paragraph(new Date().toString()));
 
            document.close();
            file.close();
 
        } catch (Exception e) {
        	System.out.println("SE FUE AL CATCH PDF");
 
            e.printStackTrace();
        }
	}
	
	public void funcion()
	{
		System.out.println("ENTRO A LA FUNCION");
	}


	public String getIdAspirante() {
		return idAspirante;
	}
	public void setIdAspirante(String idAspirante) {
		this.idAspirante = idAspirante;
	}
	public String getMostrar() {
		return mostrar;
	}
	public void setMostrar(String mostrar) {
		this.mostrar = mostrar;
	}

	public List<Inscripcion> getInscripciones() {
		return inscripciones;
	}
	public void setInscripciones(List<Inscripcion> inscripciones) {
		this.inscripciones = inscripciones;
	}


	public Aspirante getAspirante() {
		return aspirante;
	}


	public void setAspirante(Aspirante aspirante) {
		this.aspirante = aspirante;
	}


	public Convocatoria getConvocatoria() {
		return convocatoria;
	}


	public void setConvocatoria(Convocatoria convocatoria) {
		this.convocatoria = convocatoria;
	}


	public int getNumero() {
		return numero;
	}


	public void setNumero(int numero) {
		this.numero = numero;
	}


	public List<Inscripcion> getInscripciones2() {
		return inscripciones2;
	}


	public void setInscripciones2(List<Inscripcion> inscripciones2) {
		this.inscripciones2 = inscripciones2;
	}
	public String getMostrar2() {
		return mostrar2;
	}


	public void setMostrar2(String mostrar2) {
		this.mostrar2 = mostrar2;
	}


	
}

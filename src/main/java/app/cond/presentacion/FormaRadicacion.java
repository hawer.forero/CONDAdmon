package app.cond.presentacion;



import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.event.RowEditEvent;

import app.cond.entidades.Convocatoria;
import app.cond.entidades.Inscripcion;
import app.cond.negocio.IConcursoBean;

@ManagedBean
@RequestScoped
@ViewScoped
public class FormaRadicacion {
	
	@EJB 
	IConcursoBean servicio;
	
	private List<Inscripcion> inscripciones;
	private List<Inscripcion> filteredInscripciones;
	
	private List<Inscripcion> inscripciones2;
	private Convocatoria convocatoria;
	
	@PostConstruct 
	public void init() throws Exception
	{ 
		
		
		this.convocatoria = servicio.buscarConvocatoria("02-P-2014");
		this.inscripciones =  servicio.buscarInscripcionesPorConvocatoria(convocatoria);
		//System.out.println("hola etsoy con: "+inscripciones.size()); 
	}
	
	 public boolean filterByPrice(Object value, Object filter, Locale locale) 
	 {
	        String filterText = (filter == null) ? null : filter.toString().trim();
	        if(filterText == null||filterText.equals("")) {
	            return true;
	        }
	         
	        if(value == null) {
	            return false;
	        }
	         
	        return ((Comparable) value).compareTo(Integer.valueOf(filterText)) > 0;
	  }
	
	public void update(RowEditEvent event) throws Exception {
		
		try{
		Inscripcion i = (Inscripcion) event.getObject();
		servicio.editarInscripcion(i);
		}
		catch (SQLException e) 
		{
			e.printStackTrace(); 
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"LO SENTIMOS", e.getMessage()));
			
		}
    }
	public void eliminar(Convocatoria c) throws Exception 
	{
		//servicio.eliminarConvocatoria(c);
		//this.convocatorias2 =  servicio.buscarConvocatorias();
    }

	public List<Inscripcion> buscarInscripciones(String idConvocatoria) throws Exception{
		this.convocatoria = servicio.buscarConvocatoria(idConvocatoria);
		this.inscripciones = servicio.buscarInscripcionesPorConvocatoria(convocatoria);
		//this.inscripciones =  inscripciones2;
		return inscripciones;
		
	}
	

	public void onRowCancel(RowEditEvent event) {
	   	
    }

	public Convocatoria getConvocatoria() {
		return convocatoria;
	}

	public void setConvocatoria(Convocatoria convocatoria) {
		this.convocatoria = convocatoria;
	}



	public List<Inscripcion> getInscripciones2() {
		return inscripciones2;
	}



	public void setInscripciones2(List<Inscripcion> inscripciones2) {
		this.inscripciones2 = inscripciones2;
	}

	public List<Inscripcion> getFilteredInscripciones() {
		return filteredInscripciones;
	}

	public void setFilteredInscripciones(List<Inscripcion> filteredInscripciones) {
		this.filteredInscripciones = filteredInscripciones;
	}

	public List<Inscripcion> getInscripciones() {
		return inscripciones;
	}

	public void setInscripciones(List<Inscripcion> inscripciones) {
		this.inscripciones = inscripciones;
	}




	
	
	
	
	
}

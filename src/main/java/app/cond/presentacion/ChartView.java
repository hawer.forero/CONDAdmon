package app.cond.presentacion;


import javax.annotation.PostConstruct;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartSeries;

import app.cond.entidades.Concurso;
import app.cond.entidades.Convocatoria;
import app.cond.entidades.Facultad;
import app.cond.entidades.FormacionAcademica;
import app.cond.entidades.Inscripcion;
import app.cond.negocio.IConcursoBean;
 
@ManagedBean
@RequestScoped

public class ChartView implements Serializable {
    
	@EJB 
	IConcursoBean servicio;
 
    private BarChartModel animatedModel2;
    
    private List<Concurso> unidades = new ArrayList<Concurso>();
    private Convocatoria convocatoria;
    private List<Concurso> facultades = new ArrayList<Concurso>();
    private List<Concurso> concursos = new ArrayList<Concurso>();
    private List<Inscripcion> inscripciones = new ArrayList<Inscripcion>();
    
    
    private Concurso unidad;
 
    @PostConstruct
    public void init() throws Exception {
      
    	    createAnimatedModels();
    		this.convocatoria = servicio.buscarConvocatoria("02-P-2014");
    		this.facultades = servicio.agruparConcursosPorFacultad(convocatoria);
    		
    		
    		//System.out.println("hola etsoy con: "+inscripciones.size()); 
    	
    }
 
   
 
    public BarChartModel getAnimatedModel2() {
    	
        return animatedModel2;
        
    }
 
    private void createAnimatedModels() throws Exception {
         
        animatedModel2 = initBarModel();
        animatedModel2.setTitle("Numero de inscritos por concurso:");
        animatedModel2.setAnimate(true);
        animatedModel2.setLegendPosition("ne");
        Axis yAxis = animatedModel2.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(50);
    }
     
    private BarChartModel initBarModel() throws Exception {
    	
        BarChartModel model = new BarChartModel();
 
        ChartSeries inscritos = new ChartSeries();
       
        
        //this.convocatoria = servicio.buscarConvocatoria("02-P-2014");
       // this.unidades = servicio.agruparConcursosPorUnidad(convocatoria);
      
        this.concursos = servicio.buscarConcursosPorUnidad(servicio.buscarConvocatoria("02-P-2014"), servicio.buscarUnidadAcademica(1));
        this.inscripciones = servicio.buscarInscripcionesPorConvocatoria(servicio.buscarConvocatoria("02-P-2014"));
        
        inscritos.setLabel("Inscripciones");
        
       for(int i=0; i<concursos.size();i++)
        {
        	int n =0;
        	for(int j=0; j<inscripciones.size();j++)
        	{
        		if(concursos.get(i).getIdConcurso().equals(inscripciones.get(j).getConcurso().getIdConcurso()))
        		{
        			n++;
        		}
        	}
        	inscritos.set(concursos.get(i).getIdConcurso(),20+(i*10));
        }
        
       
         
        model.addSeries(inscritos);
       
        return model;
    }



	public List<Concurso> getUnidades() {
		return unidades;
	}



	public void setUnidades(List<Concurso> unidades) {
		this.unidades = unidades;
	}



	public Convocatoria getConvocatoria() {
		return convocatoria;
	}



	public void setConvocatoria(Convocatoria convocatoria) {
		this.convocatoria = convocatoria;
	}



	public List<Concurso> getFacultades() {
		return facultades;
	}



	public void setFacultades(List<Concurso> facultades) {
		this.facultades = facultades;
	}
	
	public List<Concurso> buscarUnidades(int f) throws Exception {
		List<Concurso> unidades2;
		unidades2 = servicio.agruparConcursosPorUnidad(convocatoria);
		unidades.clear();
		for(int i =0; i<unidades2.size();i++)
		{
			if(unidades2.get(i).getUnidadAcademica().getFacultad().getIdFacultad()==f)
			{
				unidades.add(unidades2.get(i));
			}
		} 
		return unidades;
		
	}



	public Concurso getUnidad() {
		return unidad;
	}



	public void setUnidad(Concurso unidad) {
		this.unidad = unidad;
	}



	public List<Concurso> getConcursos() {
		return concursos;
	}



	public void setConcursos(List<Concurso> concursos) {
		this.concursos = concursos;
	}



	public List<Inscripcion> getInscripciones() {
		return inscripciones;
	}



	public void setInscripciones(List<Inscripcion> inscripciones) {
		this.inscripciones = inscripciones;
	}
	
     
    
}
package app.cond.presentacion;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.event.RowEditEvent;
import app.cond.entidades.Concurso;
import app.cond.entidades.Convocatoria;
import app.cond.entidades.UnidadAcademica;
import app.cond.negocio.IConcursoBean;

@ManagedBean
@SessionScoped
public class FormaConcurso {
	
	@EJB 
	IConcursoBean servicio;
	
	private String idConcurso;
	private String area;
	private String idConvocatoria;
	private Integer idUnidadAcademica;
	private UnidadAcademica unidadAcademica;
	private Convocatoria convocatoria;
	private List<Concurso> concursos;
	private List<Concurso> concursos2;
	
	private ArrayList<SelectItem> unidades = new ArrayList<SelectItem>();
	
	public FormaConcurso() 
	{
		super();
	}
	@PostConstruct
    public void init() throws Exception 
    {
        this.concursos = concursos2;
       
    }

	public String abrir( Convocatoria c )
	{
		try{
			this.idConvocatoria = c.getIdConvocatoria();
			this.concursos2 = servicio.buscarConcursosPorConvocatoria(c);
			this.concursos = concursos2;
			return "viewConcursos";
		}catch (Exception e){
			System.out.println("Este e s el error: "+e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,e.getMessage(), "ERROR"));
			return "Falla";
		}	
	}
	public String guardar()
	{
		try{
			this.unidadAcademica = servicio.buscarUnidadAcademica(idUnidadAcademica);
			this.convocatoria = servicio.buscarConvocatoria(idConvocatoria);
			servicio.guardarConcurso(idConcurso, area, convocatoria, unidadAcademica);
			this.concursos = servicio.buscarConcursosPorConvocatoria(servicio.buscarConvocatoria(idConvocatoria));
			return "viewConcursos";
		}catch (Exception e){
			System.out.println("ESTE ES EL ERROR: "+e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error!", e.getMessage()));
			return "el";
		}
	}
	public void update(RowEditEvent event) throws Exception 
	{
		Concurso c = (Concurso) event.getObject();
		servicio.editaConcurso(c);
		this.concursos = servicio.buscarConcursosPorConvocatoria(servicio.buscarConvocatoria(idConvocatoria));
    }
	public void eliminar(Concurso c) throws Exception
	{
		servicio.eliminarConcurso(c);
		this.concursos = servicio.buscarConcursosPorConvocatoria(servicio.buscarConvocatoria(idConvocatoria));
	}
	public void onRowCancel(RowEditEvent event) 
	{
    	
    	
    }

	public ArrayList<SelectItem> getUnidades() throws Exception
	{
		unidades.clear();
		List<UnidadAcademica> u;
		u=servicio.buscarUnidades();
		for(int i =0; i<u.size();i++)
		{
			SelectItem e = new SelectItem(u.get(i).getIdUnidadAcademica(), u.get(i).getNombre());
			unidades.add(e);
		}
		return unidades;	
	}
	public String getIdConcurso() {
		return idConcurso;
	}
	public void setIdConcurso(String idConcurso) {
		this.idConcurso = idConcurso;
	}

	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public UnidadAcademica getUnidadAcademica() {
		return unidadAcademica;
	}
	public void setUnidadAcademica(UnidadAcademica unidadAcademica) {
		this.unidadAcademica = unidadAcademica;
	}
	public List<Concurso> getConcursos() {
		return concursos;
	}
	public void setConcursos(List<Concurso> concursos) {
		this.concursos = concursos;
	}
	public List<Concurso> getConcursos2() {
		return concursos2;
	}
	public void setConcursos2(List<Concurso> concurso2) {
		this.concursos2 = concurso2;
	}
	public void setUnidades(ArrayList<SelectItem> unidades) {
		this.unidades = unidades;
	}
	public String crearConcurso() throws Exception
	{
		return "formCrearConcurso";
	}
	public Integer getIdUnidadAcademica() {
		return idUnidadAcademica;
	}
	public void setIdUnidadAcademica(Integer idUnidadAcademica) {
		this.idUnidadAcademica = idUnidadAcademica;
	}
	public String getIdConvocatoria() {
		return idConvocatoria;
	}
	public void setIdConvocatoria(String idConvocatoria) {
		this.idConvocatoria = idConvocatoria;
	}
	public Convocatoria getConvocatoria() {
		return convocatoria;
	}
	public void setConvocatoria(Convocatoria convocatoria) {
		this.convocatoria = convocatoria;
	}
}

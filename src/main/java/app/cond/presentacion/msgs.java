package app.cond.presentacion;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class msgs {
	
	private String Usuario;
	private String Password; 
	
	public msgs(){
		this.Usuario= "El campo Usuario esta vacio";
		this.Password= "El campo Password esta vacio";
	}

	public String getUsuario() {
		return Usuario;
	}

	public void setUsuario(String usuario) {
		Usuario = usuario;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

}

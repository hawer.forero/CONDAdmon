package app.cond.presentacion;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import app.cond.entidades.Convocatoria;
import app.cond.negocio.IConcursoBean;

@ManagedBean
@SessionScoped
public class FormaMenu 
{	
	@EJB 
	IConcursoBean servicio;
	
	private String idConvocatoria;
	private ArrayList<SelectItem> convocatorias = new ArrayList<SelectItem>();
	
	public FormaMenu() {
		super();
	}
	
	public ArrayList<SelectItem> getConvocatorias() throws Exception
	{
		convocatorias.clear();
		List<Convocatoria> c;
		c=servicio.buscarConvocatorias();
		for(int i =0; i<c.size();i++)
		{
			SelectItem e = new SelectItem( c.get(i).getIdConvocatoria(), c.get(i).getIdConvocatoria());
			convocatorias.add(e);
		}
		return convocatorias;
	}
	public String Ingresar() throws Exception
	{
		
		return "index";
	}
	
	public void setConvocatorias(ArrayList<SelectItem> convocatorias) 
	{
		this.convocatorias = convocatorias;
	}
	public String getIdConvocatoria() 
	{
		return idConvocatoria;
	}
	public void setIdConvocatoria(String idConvocatoria) {
		this.idConvocatoria = idConvocatoria;
	}
}

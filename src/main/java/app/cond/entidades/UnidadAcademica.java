package app.cond.entidades;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: UnidadAcademica
 *
 */
@Entity
@Table(name="cd_unidadacademica")
@NamedQuery(name="UnidadAcademica.findAll", query="SELECT f FROM UnidadAcademica f")

public class UnidadAcademica implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer idUnidadAcademica;
	private String nombre;
	
	@ManyToOne
	@JoinColumn(name="facultad")
	private Facultad facultad;


	public UnidadAcademica() {
		super();
	}
	
	public Facultad getFacultad() {
		return facultad;
	}


	public void setFacultad(Facultad facultad) {
		this.facultad = facultad;
	}
	
	public Integer getIdUnidadAcademica() {
		return idUnidadAcademica;
	}

	public void setIdUnidadAcademica(Integer idUnidadAcademica) {
		this.idUnidadAcademica = idUnidadAcademica;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
   
}

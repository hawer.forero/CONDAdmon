package app.cond.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the cd_facultad database table.
 * 
 */
@Entity
@Table(name="cd_facultad")
@NamedQuery(name="Facultad.findAll", query="SELECT f FROM Facultad f")
public class Facultad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idFacultad;

	private String nombre;
	
	@OneToMany(mappedBy="facultad")
	private List<UnidadAcademica> unidadadesAcademicas;

	public Facultad() {
	}

	public int getIdFacultad() {
		return this.idFacultad;
	}

	public void setIdFacultad(int idFacultad) {
		this.idFacultad = idFacultad;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public List<UnidadAcademica> getUnidadadesAcademicas() {
		return unidadadesAcademicas;
	}

	public void setUnidadadesAcademicas(List<UnidadAcademica> unidadadesAcademicas) {
		this.unidadadesAcademicas = unidadadesAcademicas;
	}
	
	public UnidadAcademica addUnidadacademica(UnidadAcademica unidadAcademica) {
		
		getUnidadadesAcademicas().add(unidadAcademica);
		unidadAcademica.setFacultad(this);
		return unidadAcademica;
	}

	public UnidadAcademica removeUnidadacademica(UnidadAcademica unidadAcademica) {
		getUnidadadesAcademicas().remove(unidadAcademica);
		unidadAcademica.setFacultad(null);
		return unidadAcademica;
	}

}
package app.cond.entidades;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the cd_administrador database table.
 * 
 */
@Entity
@Table(name="cd_administrador")
@NamedQueries
({
	@NamedQuery(name="Administrador.findAll", query="SELECT a FROM Administrador a"),
	@NamedQuery( name="Administrador.buscarPorTipo", query="SELECT a FROM Administrador a WHERE a.tipo = :tipo" )
})
public class Administrador implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String usuario;

	private String cargo;

	private String dependencia;

	private String nombre;

	private String pass;

	private int tipo;
	
	public Administrador(){
		
	}

	public Administrador(String usuario, Integer tipo, String pass, String nombre, String cargo, String dependencia) {
		this.usuario = usuario;
		this.tipo = tipo;
		this.pass = pass;
		this.nombre = nombre;
		this.cargo = cargo;
		this.dependencia = dependencia;
	}
	

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getCargo() {
		return this.cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getDependencia() {
		return this.dependencia;
	}

	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public int getTipo() {
		return this.tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

}
package app.cond.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the cd_experienciaprofesional database table.
 * 
 */
@Entity
@Table(name="cd_experienciaprofesional")
@NamedQuery(name="ExperienciaProfesional.findAll", query="SELECT e FROM ExperienciaProfesional e WHERE e.aspirante = :aspirante")
public class ExperienciaProfesional implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idExperienciaProfesional;

	private String cargo;

	private String empresa;

	@Temporal(TemporalType.DATE)
	private Date fechaRetiro;

	@Temporal(TemporalType.DATE)
	private Date fechaVinculacion;

	private int folio;

	private String tipo;

	private String tipoVinculacion;
	
	@ManyToOne
	@JoinColumn(name="aspirante")
	private Aspirante aspirante;

	public Aspirante getAspirante() {
		return aspirante;
	}

	public void setAspirante(Aspirante aspirante) {
		this.aspirante = aspirante;
	}

	public ExperienciaProfesional() {
	}

	public int getIdExperienciaProfesional() {
		return idExperienciaProfesional;
	}


	public void setIdExperienciaProfesional(int idExperienciaProfesional) {
		this.idExperienciaProfesional = idExperienciaProfesional;
	}

	public String getCargo() {
		return this.cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Date getFechaRetiro() {
		return this.fechaRetiro;
	}

	public void setFechaRetiro(Date fechaRetiro) {
		this.fechaRetiro = fechaRetiro;
	}

	public Date getFechaVinculacion() {
		return this.fechaVinculacion;
	}

	public void setFechaVinculacion(Date fechaVinculacion) {
		this.fechaVinculacion = fechaVinculacion;
	}

	public int getFolio() {
		return this.folio;
	}

	public void setFolio(int folio) {
		this.folio = folio;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipoVinculacion() {
		return this.tipoVinculacion;
	}

	public void setTipoVinculacion(String tipoVinculacion) {
		this.tipoVinculacion = tipoVinculacion;
	}

}
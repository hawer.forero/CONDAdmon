package app.cond.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Inscripcion
 *
 */
@Entity
@Table(name="cd_inscripcion")
@NamedQueries
({
	@NamedQuery(name="Inscripcion.findAll", query="SELECT i FROM Inscripcion i WHERE i.concurso.convocatoria = :convocatoria"),
	@NamedQuery( name="Inscripcion.buscarPorAspirante", query="SELECT i FROM Inscripcion i WHERE i.concurso.convocatoria =:convocatoria AND i.aspirante = :aspirante ORDER BY i.concurso.unidadAcademica,i.concurso"  )
	
})

public class Inscripcion implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@Id
	private int idInscripcion;
	
	@Temporal(TemporalType.DATE)
	private Date fechaInscripcion;
	
	private String radico;
	
   // @Temporal(TemporalType.DATE)
	//private Date fechaRadicado;
	private String fechaRadicado;
	
	private String numero;
	
	@ManyToOne
	@JoinColumn(name="aspirante")
	private Aspirante aspirante;
	
	@ManyToOne
	@JoinColumn(name="concurso")
	private Concurso concurso;
	
	
	
	
	public int getIdInscripcion() {
		return idInscripcion;
	}




	public void setIdInscripcion(int idInscripcion) {
		this.idInscripcion = idInscripcion;
	}




	public Date getFechaInscripcion() {
		return fechaInscripcion;
	}




	public void setFechaInscripcion(Date fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}




	public String getRadico() {
		return radico;
	}




	public void setRadico(String radico) {
		this.radico = radico;
	}



    public String getFechaRadicado() {
		return fechaRadicado;
	}


	public void setFechaRadicado(String fechaRadicado) {
		this.fechaRadicado = fechaRadicado;
	}




	/*
	public Date getFechaRadicado() {
		return fechaRadicado;
	}




	public void setFechaRadicado(Date fechaRadicado) {
		this.fechaRadicado = fechaRadicado;
	}

*/
	public Aspirante getAspirante() {
		return aspirante;
	}




	public void setAspirante(Aspirante aspirante) {
		this.aspirante = aspirante;
	}




	public Concurso getConcurso() {
		return concurso;
	}




	public void setConcurso(Concurso concurso) {
		this.concurso = concurso;
	}




	public String getNumero() {
		return numero;
	}




	public void setNumero(String numero) {
		this.numero = numero;
	}

   
}

package app.cond.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the cd_productividadintelectual database table.
 * 
 */
@Entity
@Table(name="cd_productividadintelectual")
@NamedQuery(name="ProductividadIntelectual.findAll", query="SELECT p FROM ProductividadIntelectual p WHERE p.aspirante =:aspirante")
public class ProductividadIntelectual implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idProductividadIntelectual;

	private String anio;

	private int autores;

	private int autoresTraduccion;

	private String categoria;

	private String editorial;

	private String editorialTraduccion;

	@Temporal(TemporalType.DATE)
	private Date fechaPublicacion;

	@Temporal(TemporalType.DATE)
	private Date fechaPublicacionTraducccion;

	@Temporal(TemporalType.DATE)
	private Date fechaTerminacion;

	private int folio;
	
	private int folio2;

	private String institucion;

	private String isbn;

	private String issn;

	private String nombre;

	private String nombreTraduccion;

	private int numero;

	private String revista;

	private int tipo;

	private int volumen;
	
	@ManyToOne
	@JoinColumn(name="aspirante")
	private Aspirante aspirante;

	public ProductividadIntelectual() {
	}

	public int getIdProductividadIntelectual() {
		return idProductividadIntelectual;
	}

	public Aspirante getAspirante() {
		return aspirante;
	}

	public void setAspirante(Aspirante aspirante) {
		this.aspirante = aspirante;
	}

	public void setIdProductividadIntelectual(int idProductividadIntelectual) {
		this.idProductividadIntelectual = idProductividadIntelectual;
	}

    

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public int getAutores() {
		return this.autores;
	}

	public void setAutores(int autores) {
		this.autores = autores;
	}

	public int getAutoresTraduccion() {
		return this.autoresTraduccion;
	}

	public void setAutoresTraduccion(int autoresTraduccion) {
		this.autoresTraduccion = autoresTraduccion;
	}

	public String getCategoria() {
		return this.categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getEditorial() {
		return this.editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public String getEditorialTraduccion() {
		return this.editorialTraduccion;
	}

	public void setEditorialTraduccion(String editorialTraduccion) {
		this.editorialTraduccion = editorialTraduccion;
	}

	public Date getFechaPublicacion() {
		return this.fechaPublicacion;
	}

	public void setFechaPublicacion(Date fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public Date getFechaPublicacionTraducccion() {
		return this.fechaPublicacionTraducccion;
	}

	public void setFechaPublicacionTraducccion(Date fechaPublicacionTraducccion) {
		this.fechaPublicacionTraducccion = fechaPublicacionTraducccion;
	}

	public Date getFechaTerminacion() {
		return this.fechaTerminacion;
	}

	public void setFechaTerminacion(Date fechaTerminacion) {
		this.fechaTerminacion = fechaTerminacion;
	}

	public int getFolio() {
		return this.folio;
	}

	public void setFolio(int folio) {
		this.folio = folio;
	}

	public String getInstitucion() {
		return this.institucion;
	}

	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}

	


	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreTraduccion() {
		return this.nombreTraduccion;
	}

	public void setNombreTraduccion(String nombreTraduccion) {
		this.nombreTraduccion = nombreTraduccion;
	}

	public int getNumero() {
		return this.numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getRevista() {
		return this.revista;
	}

	public void setRevista(String revista) {
		this.revista = revista;
	}

	public int getTipo() {
		return this.tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getVolumen() {
		return this.volumen;
	}

	public void setVolumen(int volumen) {
		this.volumen = volumen;
	}

	public int getFolio2() {
		return folio2;
	}

	public void setFolio2(int folio2) {
		this.folio2 = folio2;
	}

}
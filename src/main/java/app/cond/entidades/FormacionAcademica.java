package app.cond.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the cd_formacionacademica database table.
 * 
 */
@Entity
@Table(name="cd_formacionacademica")
@NamedQuery(name="FormacionAcademica.findAll", query="SELECT f FROM FormacionAcademica f WHERE f.aspirante = :aspirante")
public class FormacionAcademica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer idFormacionAcademica;

	@Temporal(TemporalType.DATE)
	private Date fechaGrado;

	private String graduado;

	private String modalidad;

	private String numeroTarjeta;
	
	private String semestresAprobados;

	private String titulo;

	@ManyToOne
	@JoinColumn(name="aspirante")
	private Aspirante aspirante;
	
	public FormacionAcademica() {
	}

	public Date getFechaGrado() {
		return this.fechaGrado;
	}

	public void setFechaGrado(Date fechaGrado) {
		this.fechaGrado = fechaGrado;
	}

	public String getGraduado() {
		return this.graduado;
	}

	public void setGraduado(String graduado) {
		this.graduado = graduado;
	}

	public String getModalidad() {
		return this.modalidad;
	}

	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}

	public String getNumeroTarjeta() {
		return this.numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	

	public String getSemestresAprobados() {
		return semestresAprobados;
	}

	public void setSemestresAprobados(String semestresAprobados) {
		this.semestresAprobados = semestresAprobados;
	}

	public String getTitulo() {
		return this.titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public void setIdFormacionAcademica(Integer idFormacionAcademica) {
		this.idFormacionAcademica = idFormacionAcademica;
	}

	public Aspirante getAspirante() {
		return aspirante;
	}

	public void setAspirante(Aspirante aspirante) {
		this.aspirante = aspirante;
	}
	public Integer getIdFormacionAcademica() {
		return idFormacionAcademica;
	}

}
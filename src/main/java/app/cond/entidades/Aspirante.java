package app.cond.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the cd_aspirante database table.
 * 
 */
@Entity
@Table(name="cd_aspirante")
@NamedQuery(name="Aspirante.findAll", query="SELECT a FROM Aspirante a")
public class Aspirante implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String idAspirante;

	private String apellido1;

	private String apellido2;

	private String email;

	private String nombre;

	private String pass;

	private String tipoId;
	
	@OneToOne(mappedBy="aspirante")
	private InformacionPersonal informacionPersonal;
	
	@OneToMany(mappedBy="aspirante")
	private List<FormacionAcademica> formacionAcademica;
	
	@OneToMany(mappedBy="aspirante")
	private List<SegundaLengua> segundaLengua;
	
	@OneToMany(mappedBy="aspirante")
	private List<ExperienciaDocente> experienciaDocente;
	
	@OneToMany(mappedBy="aspirante")
	private List<ExperienciaProfesional> experienciaProfesional;
	
	@OneToMany(mappedBy="aspirante")
	private List<ProductividadIntelectual> productividadIntelectual;
	
	@OneToMany(mappedBy="aspirante")
	private List<Inscripcion> inscripcion;
	

	

	public List<Inscripcion> getInscripcion() {
		return inscripcion;
	}

	public void setInscripcion(List<Inscripcion> inscripcion) {
		this.inscripcion = inscripcion;
	}

	public List<ProductividadIntelectual> getProductividadIntelectual() {
		return productividadIntelectual;
	}

	public void setProductividadIntelectual(
			List<ProductividadIntelectual> productividadIntelectual) {
		this.productividadIntelectual = productividadIntelectual;
	}

	public List<ExperienciaProfesional> getExperienciaProfesional() {
		return experienciaProfesional;
	}

	public void setExperienciaProfesional(
			List<ExperienciaProfesional> experienciaProfesional) {
		this.experienciaProfesional = experienciaProfesional;
	}

	public List<ExperienciaDocente> getExperienciaDocente() {
		return experienciaDocente;
	}

	public void setExperienciaDocente(List<ExperienciaDocente> experienciaDocente) {
		this.experienciaDocente = experienciaDocente;
	}

	public Aspirante() {
	}

	public String getIdAspirante() {
		return this.idAspirante;
	}

	public void setIdAspirante(String idAspirante) {
		this.idAspirante = idAspirante;
	}

	public String getApellido1() {
		return this.apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return this.apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getTipoId() {
		return this.tipoId;
	}

	public void setTipoId(String tipoId) {
		this.tipoId = tipoId;
	}
	
	public List<FormacionAcademica> getFormacionAcademica() {
		return formacionAcademica;
	}

	public void setFormacionAcademica(List<FormacionAcademica> formacionAcademica) {
		this.formacionAcademica = formacionAcademica;
	}

	public InformacionPersonal getInformacionPersonal() {
		return informacionPersonal;
	}

	public void setInformacionPersonal(InformacionPersonal informacionPersonal) {
		this.informacionPersonal = informacionPersonal;
	}
	public List<SegundaLengua> getSegundaLengua() {
		return segundaLengua;
	}

	public void setSegundaLengua(List<SegundaLengua> segundaLengua) {
		this.segundaLengua = segundaLengua;
	}
	
	public FormacionAcademica addFormacionAcademica(FormacionAcademica formacionAcademica) {
		
		getFormacionAcademica().add(formacionAcademica);
		formacionAcademica.setAspirante(this);
		return formacionAcademica;
	}

	public FormacionAcademica removeFormacionAcademica(FormacionAcademica formacionAcademica) {
		getFormacionAcademica().remove(formacionAcademica);
		formacionAcademica.setAspirante(null);
		return formacionAcademica;
	}
	
    public SegundaLengua addSegundaLengua(SegundaLengua segundaLengua) {
		
		getSegundaLengua().add(segundaLengua);
		segundaLengua.setAspirante(this);
		return segundaLengua;
	}

	public SegundaLengua removeSegundaLengua(SegundaLengua segundaLengua) {
		getSegundaLengua().remove(segundaLengua);
		segundaLengua.setAspirante(null);
		return segundaLengua;
	}
	
	public ExperienciaDocente addExperienciaDocente(ExperienciaDocente experienciaDocente) {
		
		getExperienciaDocente().add(experienciaDocente);
		experienciaDocente.setAspirante(this);
		return experienciaDocente;
	}

	public ExperienciaDocente removeExperienciaDocente(ExperienciaDocente experienciaDocente) {
		getExperienciaDocente().remove(experienciaDocente);
		experienciaDocente.setAspirante(null);
		return experienciaDocente;
	}
	
	public ExperienciaProfesional addExperienciaProfesional(ExperienciaProfesional experienciaProfesional) {
		
		getExperienciaProfesional().add(experienciaProfesional);
		experienciaProfesional.setAspirante(this);
		return experienciaProfesional;
	}

	public ExperienciaProfesional removeExperienciaProfesional(ExperienciaProfesional experienciaProfesional) {
		getExperienciaProfesional().remove(experienciaProfesional);
		experienciaProfesional.setAspirante(null);
		return experienciaProfesional;
	}
	
	public ProductividadIntelectual addProductividadIntelectual(ProductividadIntelectual productividadIntelectual) {
		
		getProductividadIntelectual().add(productividadIntelectual);
		productividadIntelectual.setAspirante(this);
		return productividadIntelectual;
	}

	public ProductividadIntelectual removeProductividadIntelectual(ProductividadIntelectual productividadIntelectual) {
		getProductividadIntelectual().remove(productividadIntelectual);
		productividadIntelectual.setAspirante(null);
		return productividadIntelectual;
	}
	
public Inscripcion addInscripcion(Inscripcion inscripcion) {
		
		getInscripcion().add(inscripcion);
		inscripcion.setAspirante(this);
		return inscripcion;
	}

	public Inscripcion removeInscripcion(Inscripcion inscripcion) {
		getInscripcion().remove(inscripcion);
		inscripcion.setAspirante(null);
		return inscripcion;
	}

}
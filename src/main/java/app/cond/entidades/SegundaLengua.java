package app.cond.entidades;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the cd_segundalengua database table.
 * 
 */
@Entity
@Table(name="cd_segundalengua")
@NamedQuery(name="SegundaLengua.findAll", query="SELECT s FROM SegundaLengua s WHERE s.aspirante =:aspirante")
public class SegundaLengua implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer idSegundaLengua;

	private String escribe;

	private String habla;

	private String lee;

	private String nombre;
	
	@ManyToOne
	@JoinColumn(name="aspirante")
	private Aspirante aspirante;

	public Aspirante getAspirante() {
		return aspirante;
	}

	public void setAspirante(Aspirante aspirante) {
		this.aspirante = aspirante;
	}

	public SegundaLengua() {
	}

	public Integer getIdSegundaLengua() {
		return idSegundaLengua;
	}

	public void setIdSegundaLengua(Integer idSegundaLengua) {
		this.idSegundaLengua = idSegundaLengua;
	}

	public String getEscribe() {
		return this.escribe;
	}

	public void setEscribe(String escribe) {
		this.escribe = escribe;
	}

	public String getHabla() {
		return this.habla;
	}

	public void setHabla(String habla) {
		this.habla = habla;
	}

	public String getLee() {
		return this.lee;
	}

	public void setLee(String lee) {
		this.lee = lee;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
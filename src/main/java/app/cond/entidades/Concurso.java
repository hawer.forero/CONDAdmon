package app.cond.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Concurso
 *
 */
@Entity
@Table(name="cd_concurso")
@NamedQueries
({
	@NamedQuery(name="Concurso.findAll", query="SELECT c FROM Concurso c"),
	@NamedQuery( name="Concurso.buscarPorConvocatoria", query="SELECT a FROM Concurso a WHERE a.convocatoria = :convocatoria ORDER BY a.unidadAcademica,a.idConcurso" ),
	@NamedQuery( name="Concurso.buscarPorUnidad", query="SELECT a FROM Concurso a WHERE a.convocatoria = :convocatoria AND a.unidadAcademica = :unidad ORDER BY a.unidadAcademica,a.idConcurso" ),
	@NamedQuery( name="Concurso.agruparPorFacultad", query="SELECT a FROM Concurso a WHERE a.convocatoria = :convocatoria GROUP BY a.unidadAcademica.facultad"),
	@NamedQuery( name="Concurso.agruparPorUnidad", query="SELECT a FROM Concurso a WHERE a.convocatoria = :convocatoria GROUP BY a.unidadAcademica")
})
public class Concurso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String idConcurso;

	private String area;

	//bi-directional many-to-one association to Convocatoria
	@ManyToOne
	@JoinColumn(name="convocatoria")
	private Convocatoria convocatoria;

	//bi-directional many-to-one association to Unidadacademica
	@ManyToOne
	@JoinColumn(name="unidadacademica")
	private UnidadAcademica unidadAcademica;

	@OneToMany(mappedBy="concurso")
	private List<Inscripcion> inscripcion;
	
	public List<Inscripcion> getInscripcion() {
		return inscripcion;
	}
	public void setInscripcion(List<Inscripcion> inscripcion) {
		this.inscripcion = inscripcion;
	}
	public void setUnidadAcademica(UnidadAcademica unidadAcademica) {
		this.unidadAcademica = unidadAcademica;
	}
	public Concurso(){
		
	}
	public Concurso(String idConcurso, String area, Convocatoria convocatoria, UnidadAcademica unidadAcademica) {
		this.idConcurso = idConcurso;
		this.area = area;
		this.convocatoria = convocatoria;
		this.unidadAcademica = unidadAcademica;
	}

	public String getIdConcurso() {
		return this.idConcurso;
	}

	public void setIdConcurso(String idConcurso) {
		this.idConcurso = idConcurso;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Convocatoria getConvocatoria() {
		return this.convocatoria;
	}

	public void setConvocatoria(Convocatoria convocatoria) {
		this.convocatoria = convocatoria;
	}

	public UnidadAcademica getUnidadAcademica() {
		return this.unidadAcademica;
	}

	public void setUnidadacademica(UnidadAcademica unidadAcademica) {
		this.unidadAcademica = unidadAcademica;
	}

}
package app.cond.entidades;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

@Entity

@Table(name="cd_convocatoria")

@NamedQuery(name="Convocatoria.findAll", query="SELECT c FROM Convocatoria c ORDER BY c.anio,c.idConvocatoria,c.periodo DESC")
public class Convocatoria implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private String idConvocatoria;
	private int anio;
	private String etapa;
	private int periodo;
	private String tipo;
	
	//bi-directional many-to-one association to Concurso
	@OneToMany(mappedBy="convocatoria")
	private List<Concurso> concursos;

	public Convocatoria() {}
	
	public Convocatoria (String idConvocatoria, String tipo, Integer año, Integer periodo, String etapa){
		super();
		this.idConvocatoria = idConvocatoria;
		this.tipo = tipo;
		this.anio = año;
		this.periodo = periodo;
		this.etapa = etapa;
	}
	public String getIdConvocatoria() {
		return this.idConvocatoria;
	}
	public void setIdConvocatoria(String idConvocatoria) {
		this.idConvocatoria = idConvocatoria;
	}
	public int getAnio() {
		return this.anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public String getEtapa() {
		return this.etapa;
	}
	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}
	public int getPeriodo() {
		return this.periodo;
	}
	public void setPeriodo(int periodo) {
		this.periodo = periodo;
	}
	public String getTipo() {
		return this.tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public List<Concurso> getConcursos() {
		return this.concursos;
	}
	public void setConcursos(List<Concurso> concursos) {
		this.concursos = concursos;
	}
	public Concurso addConcurso(Concurso concurso) {
		getConcursos().add(concurso);
		concurso.setConvocatoria(this);
		return concurso;
	}
	public Concurso removeConcurso(Concurso concurso) {
		getConcursos().remove(concurso);
		concurso.setConvocatoria(null);
		return concurso;
	}
}
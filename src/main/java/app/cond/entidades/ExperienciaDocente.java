package app.cond.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the cd_experienciadocente database table.
 * 
 */
@Entity
@Table(name="cd_experienciadocente")
@NamedQuery(name="ExperienciaDocente.findAll", query="SELECT e FROM ExperienciaDocente e WHERE e.aspirante =:aspirante")
public class ExperienciaDocente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idExperienciaDocente;

	private String dedicacion;

	@Temporal(TemporalType.DATE)
	private Date fechaRetiro;

	@Temporal(TemporalType.DATE)
	private Date fechaVinculacion;

	private Integer folio;

	private int horasSemana;

	private int horasSemestre;

	private String institucion;

	private int semanasSemestre;
	
	@ManyToOne
	@JoinColumn(name="aspirante")
	private Aspirante aspirante;

	public Aspirante getAspirante() {
		return aspirante;
	}




	public void setAspirante(Aspirante aspirante) {
		this.aspirante = aspirante;
	}
	
	public ExperienciaDocente() {
	}

	public int getIdExperienciaDocente() {
		return idExperienciaDocente;
	}

	public void setIdExperienciaDocente(int idExperienciaDocente) {
		this.idExperienciaDocente = idExperienciaDocente;
	}

	public String getDedicacion() {
		return this.dedicacion;
	}

	public void setDedicacion(String dedicacion) {
		this.dedicacion = dedicacion;
	}

	public Date getFechaRetiro() {
		return this.fechaRetiro;
	}

	public void setFechaRetiro(Date fechaRetiro) {
		this.fechaRetiro = fechaRetiro;
	}

	public Date getFechaVinculacion() {
		return this.fechaVinculacion;
	}

	public void setFechaVinculacion(Date fechaVinculacion) {
		this.fechaVinculacion = fechaVinculacion;
	}

	public Integer getFolio() {
		return folio;
	}
	public void setFolio(Integer folio) {
		this.folio = folio;
	}
	public int getHorasSemana() {
		return this.horasSemana;
	}

	public void setHorasSemana(int horasSemana) {
		this.horasSemana = horasSemana;
	}

	public int getHorasSemestre() {
		return this.horasSemestre;
	}

	public void setHorasSemestre(int horasSemestre) {
		this.horasSemestre = horasSemestre;
	}

	public String getInstitucion() {
		return this.institucion;
	}

	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}

	public int getSemanasSemestre() {
		return this.semanasSemestre;
	}

	public void setSemanasSemestre(int semanasSemestre) {
		this.semanasSemestre = semanasSemestre;
	}

}
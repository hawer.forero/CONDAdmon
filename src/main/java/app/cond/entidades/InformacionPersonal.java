package app.cond.entidades;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the cd_informacionpersonal database table.
 * 
 */
@Entity
@Table(name="cd_informacionpersonal")
@NamedQuery(name="InformacionPersonal.findAll", query="SELECT i FROM InformacionPersonal i WHERE i.aspirante = :aspirante")
public class InformacionPersonal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer idInformacionPersonal;

	private String barrio;

	private BigInteger celular1;

	private BigInteger celular2;

	private String depCorrespondencia;

	private String depNacimiento;

	private String direccion;
	
	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;

	private String idioma;

	private String munCorrespondencia;

	private String munNacimiento;

	private String nacionalidad;

	private String paisCorrespondencia;

	private String paisNacimiento;

	private String sexo;

	private Integer telefono;
	
	@OneToOne
	@JoinColumn(name="aspirante")
	private Aspirante aspirante;

	public Integer getIdInformacionPersonal() {
		return idInformacionPersonal;
	}

	public void setIdInformacionPersonal(Integer idInformacionPersonal) {
		this.idInformacionPersonal = idInformacionPersonal;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}



	public BigInteger getCelular1() {
		return celular1;
	}

	public void setCelular1(BigInteger celular1) {
		this.celular1 = celular1;
	}

	public BigInteger getCelular2() {
		return celular2;
	}

	public void setCelular2(BigInteger celular2) {
		this.celular2 = celular2;
	}

	public String getDepCorrespondencia() {
		return depCorrespondencia;
	}

	public void setDepCorrespondencia(String depCorrespondencia) {
		this.depCorrespondencia = depCorrespondencia;
	}

	public String getDepNacimiento() {
		return depNacimiento;
	}

	public void setDepNacimiento(String depNacimiento) {
		this.depNacimiento = depNacimiento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getMunCorrespondencia() {
		return munCorrespondencia;
	}

	public void setMunCorrespondencia(String munCorrespondencia) {
		this.munCorrespondencia = munCorrespondencia;
	}

	public String getMunNacimiento() {
		return munNacimiento;
	}

	public void setMunNacimiento(String munNacimiento) {
		this.munNacimiento = munNacimiento;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getPaisCorrespondencia() {
		return paisCorrespondencia;
	}

	public void setPaisCorrespondencia(String paisCorrespondencia) {
		this.paisCorrespondencia = paisCorrespondencia;
	}

	public String getPaisNacimiento() {
		return paisNacimiento;
	}

	public void setPaisNacimiento(String paisNacimiento) {
		this.paisNacimiento = paisNacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Integer getTelefono() {
		return telefono;
	}

	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}

	public Aspirante getAspirante() {
		return aspirante;
	}

	public void setAspirante(Aspirante aspirante) {
		this.aspirante = aspirante;
	}

	public InformacionPersonal() {
	}

	

}
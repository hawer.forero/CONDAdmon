package app.cond.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import app.cond.entidades.Aspirante;
import app.cond.entidades.ExperienciaProfesional;

public class ExperienciaProfesionalDAO {

	@PersistenceContext
	EntityManager manager;
	
   public List<ExperienciaProfesional> buscarExperienciaProfesional(Aspirante a){
	   
	   TypedQuery<ExperienciaProfesional> consulta;
	   consulta = manager.createNamedQuery("ExperienciaProfesional.findAll", ExperienciaProfesional.class);
	   consulta.setParameter("aspirante", a);
	   return consulta.getResultList();  
   }

}

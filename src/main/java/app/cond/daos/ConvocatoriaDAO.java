package app.cond.daos;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import app.cond.entidades.Convocatoria;

public class ConvocatoriaDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public Convocatoria buscarConvocatoria(String idConvocatoria)
	{
		return manager.find(Convocatoria.class, idConvocatoria);
	}
	public List<Convocatoria> buscarConvocatorias()
	{
		TypedQuery<Convocatoria> consulta;
		consulta = manager.createNamedQuery( "Convocatoria.findAll", Convocatoria.class );
		return consulta.getResultList();	
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void guardarConvocatoria(Convocatoria c){
		manager.persist(c);
		
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void eliminarConvocatoria(Convocatoria c){
		manager.remove(c);
		
	}
	/*public void editarConvocatoria(Convocatoria c){
		manager.refresh(c);
		
	}*/
	
}

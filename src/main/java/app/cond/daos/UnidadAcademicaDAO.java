package app.cond.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import app.cond.entidades.UnidadAcademica;

public class UnidadAcademicaDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public UnidadAcademica buscarUnidadAcademica(Integer idUnidadAcademica)
	{
		return manager.find(UnidadAcademica.class, idUnidadAcademica);
	}
	public List<UnidadAcademica> buscarUnidades()
	{
		TypedQuery<UnidadAcademica> consulta;
		consulta = manager.createNamedQuery( "UnidadAcademica.findAll", UnidadAcademica.class );
		return consulta.getResultList();	
	}

}

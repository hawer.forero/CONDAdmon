package app.cond.daos;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import app.cond.entidades.Facultad;

public class FacultadDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public Facultad buscarFacultad(Integer idFacultad)
	{
		return manager.find(Facultad.class, idFacultad);
	}

}

package app.cond.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import app.cond.entidades.Aspirante;
import app.cond.entidades.SegundaLengua;

public class SegundaLenguaDAO {
	@PersistenceContext
	EntityManager manager;
	
   public List<SegundaLengua> buscarIdiomas( Aspirante a){
	   
	   TypedQuery<SegundaLengua> consulta;
	   consulta = manager.createNamedQuery("SegundaLengua.findAll", SegundaLengua.class);
	   consulta.setParameter("aspirante", a);
	   return consulta.getResultList();  
   }

}

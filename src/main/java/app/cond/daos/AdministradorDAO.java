package app.cond.daos;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import app.cond.entidades.Administrador;




public class AdministradorDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public Administrador buscarUsuario(String usuario)
	{
		return manager.find(Administrador.class, usuario);
	}
	
	public List<Administrador> buscarUsuarios()
	{
		TypedQuery<Administrador> consulta;
		consulta = manager.createNamedQuery( "Administrador.findAll", Administrador.class );
		
		return consulta.getResultList();
	}
	public List<Administrador> buscarUsuariosPorTipo(Integer tipo)
	{
		TypedQuery<Administrador> consulta;
		consulta = manager.createNamedQuery( "Administrador.buscarPorTipo", Administrador.class);
		consulta.setParameter("tipo", tipo);
		
		return consulta.getResultList();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void guardarUsuario(Administrador u){
		manager.persist(u);
		
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void eliminarUsuario(Administrador u){
		manager.remove(u);
		
	}
}

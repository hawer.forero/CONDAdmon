package app.cond.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import app.cond.entidades.Aspirante;


public class AspiranteDAO {
	
	@PersistenceContext
	EntityManager manager;
	
   public List<Aspirante> buscarAspirantes(){
	   
	   TypedQuery<Aspirante> listaAspirante;
	   listaAspirante = manager.createNamedQuery("Aspirante.findAll", Aspirante.class);
	   return listaAspirante.getResultList();  
   }
   
   public Aspirante buscarAspirante(String idAspirante)
	{
		return manager.find(Aspirante.class, idAspirante);
	}
   
   public void guardarAspirante(Aspirante a){
	   
	   manager.persist(a); 
   }
	
}

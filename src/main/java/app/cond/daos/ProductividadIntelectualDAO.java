package app.cond.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import app.cond.entidades.Aspirante;
import app.cond.entidades.ProductividadIntelectual;

public class ProductividadIntelectualDAO {

	@PersistenceContext
	EntityManager manager;
	
   public List<ProductividadIntelectual> buscarProductividadIntelectual(Aspirante a){
	   
	   TypedQuery<ProductividadIntelectual> consulta;
	   consulta = manager.createNamedQuery("ProductividadIntelectual.findAll", ProductividadIntelectual.class);
	   consulta.setParameter("aspirante", a);
	   return consulta.getResultList();  
   }

}

package app.cond.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import app.cond.entidades.Aspirante;
import app.cond.entidades.Convocatoria;
import app.cond.entidades.Inscripcion;


public class InscripcionDAO {
	@PersistenceContext
	EntityManager manager;
	
	
	public Inscripcion buscarInscripcion(Integer idInscripcion)
	{
		return manager.find(Inscripcion.class, idInscripcion);
	}
   public List<Inscripcion> buscarInscripciones(Convocatoria c){
	   
	   TypedQuery<Inscripcion> consulta;
	   consulta = manager.createNamedQuery("Inscripcion.findAll", Inscripcion.class);
	   consulta.setParameter("convocatoria", c);
	   return consulta.getResultList();  
   }
   
   public  List<Inscripcion> buscarInscripcionesPorAspirante(Convocatoria c, Aspirante a)
	{
		TypedQuery<Inscripcion> consulta;
		consulta = manager.createNamedQuery( "Inscripcion.buscarPorAspirante", Inscripcion.class);
		consulta.setParameter("convocatoria", c);
		consulta.setParameter("aspirante", a);
		return consulta.getResultList();
	}

}

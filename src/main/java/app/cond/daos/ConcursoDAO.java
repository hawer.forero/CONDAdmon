package app.cond.daos;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import app.cond.entidades.Concurso;
import app.cond.entidades.Convocatoria;
import app.cond.entidades.UnidadAcademica;



public class ConcursoDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public Concurso buscarConcurso(String concurso)
	{
		return manager.find(Concurso.class, concurso);
	}
	public  List<Concurso> buscarConcursoPorConvocatoria(Convocatoria c)
	{
		TypedQuery<Concurso> consulta;
		consulta = manager.createNamedQuery( "Concurso.buscarPorConvocatoria", Concurso.class);
		consulta.setParameter("convocatoria", c);
		return consulta.getResultList();
	}
	public  List<Concurso> buscarConcursoPorUnidad(Convocatoria c, UnidadAcademica u)
	{
		TypedQuery<Concurso> consulta;
		consulta = manager.createNamedQuery( "Concurso.buscarPorUnidad", Concurso.class);
		consulta.setParameter("convocatoria", c);
		consulta.setParameter("unidad", u);
		return consulta.getResultList();
	}
	public  List<Concurso> agruparConcursoPorFacultad(Convocatoria c)
	{
		TypedQuery<Concurso> consulta;
		consulta = manager.createNamedQuery( "Concurso.agruparPorFacultad", Concurso.class);
		consulta.setParameter("convocatoria", c);
		return consulta.getResultList();
	}
	public  List<Concurso> agruparConcursoPorUnidad(Convocatoria c)
	{
		TypedQuery<Concurso> consulta;
		consulta = manager.createNamedQuery( "Concurso.agruparPorUnidad", Concurso.class);
		consulta.setParameter("convocatoria", c);
		return consulta.getResultList();
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void guardarConcurso(Concurso c){
		manager.persist(c);
		
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void eliminarConcurso(Concurso c){
		manager.remove(c);
	}

}

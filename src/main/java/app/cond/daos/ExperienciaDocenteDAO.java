package app.cond.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import app.cond.entidades.Aspirante;
import app.cond.entidades.ExperienciaDocente;

public class ExperienciaDocenteDAO {

	@PersistenceContext
	EntityManager manager;
	
   public List<ExperienciaDocente> buscarExperienciaDocente(Aspirante a){
	   
	   TypedQuery<ExperienciaDocente> consulta;
	   consulta = manager.createNamedQuery("ExperienciaDocente.findAll", ExperienciaDocente.class);
	   consulta.setParameter("aspirante", a);
	   return consulta.getResultList();  
   }

}

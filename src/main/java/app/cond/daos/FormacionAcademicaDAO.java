package app.cond.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import app.cond.entidades.Aspirante;
import app.cond.entidades.FormacionAcademica;


public class FormacionAcademicaDAO {
	@PersistenceContext
	EntityManager manager;
	
   public List<FormacionAcademica> buscarFormAcademica(Aspirante a){
	   
	   TypedQuery<FormacionAcademica> consulta;
	   consulta = manager.createNamedQuery("FormacionAcademica.findAll", FormacionAcademica.class);
	   consulta.setParameter("aspirante", a);
	   return consulta.getResultList();  
   }

}

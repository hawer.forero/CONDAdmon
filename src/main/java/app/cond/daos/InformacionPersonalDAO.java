package app.cond.daos;



import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import app.cond.entidades.Aspirante;
import app.cond.entidades.InformacionPersonal;

public class InformacionPersonalDAO {
	@PersistenceContext
	EntityManager manager;
	
   public InformacionPersonal buscarInfoPersonal( Aspirante a){
	   
	   TypedQuery<InformacionPersonal> consulta;
	   consulta = manager.createNamedQuery("InformacionPersonal.findAll", InformacionPersonal.class);
	   consulta.setParameter("aspirante", a);
	   return consulta.getSingleResult();
   }
   
   

}

package app.cond.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;


import app.cond.daos.*;
import app.cond.entidades.Administrador;
import app.cond.entidades.Aspirante;
import app.cond.entidades.Concurso;
import app.cond.entidades.Convocatoria;
import app.cond.entidades.ExperienciaDocente;
import app.cond.entidades.ExperienciaProfesional;
import app.cond.entidades.FormacionAcademica;
import app.cond.entidades.InformacionPersonal;
import app.cond.entidades.Inscripcion;
import app.cond.entidades.ProductividadIntelectual;
import app.cond.entidades.SegundaLengua;
import app.cond.entidades.UnidadAcademica;

/**
 * Session Bean implementation class ConcursoBean
 */
@Stateless
public class ConcursoBean implements IConcursoBean 
{
   
	@Inject
	  AdministradorDAO usuarioDAO;
	@Inject
	  ConvocatoriaDAO convocatoriaDAO;
	@Inject
	  ConcursoDAO concursoDAO;
	@Inject
	  UnidadAcademicaDAO unidadAcademicaDAO;
	@Inject
	  AspiranteDAO aspiranteDAO;
	@Inject
	  InscripcionDAO inscripcionDAO;
	@Inject
	  InformacionPersonalDAO informacionPersonalDAO;
	@Inject
	  FormacionAcademicaDAO formacionAcademicaDAO;
	@Inject
	  SegundaLenguaDAO segundaLenguaDAO;
	@Inject
	  ExperienciaDocenteDAO experienciaDocenteDAO;
	@Inject
	  ExperienciaProfesionalDAO experienciaProfesionalDAO;
	@Inject
	  ProductividadIntelectualDAO productividadIntelectualDAO;
	
	
    public ConcursoBean() {
        
    }
    
    //LOGIN
    public  Integer validarUsuario(String usuario2) throws Exception
    {
    	Integer result = 0;
    	Administrador usuario= usuarioDAO.buscarUsuario(usuario2);
    	if(usuario != null)
    	{
    		result = 1;
    	}
		return result;		
    }
    public  Integer validarPass(String usuario, String password) throws Exception
    {
    	Integer result = 0;
    	Administrador us= usuarioDAO.buscarUsuario(usuario);
    	if(us.getPass().equals(password))
    	{
    		result = 1;
    	}
    	return result;
    }
    public  Integer validarSesion(String usuario, Integer tipo) throws Exception
    {
    	Integer result = 0;
    	Administrador us= usuarioDAO.buscarUsuario(usuario);
    	if(us.getTipo()==tipo)
    	{
    		result = 1;
    	}
    	return result;
    }
    
   //CONVOCATORIA
	public Convocatoria buscarConvocatoria(String idConvocatoria)
	{
		return convocatoriaDAO.buscarConvocatoria(idConvocatoria);
	}
	public List<Convocatoria> buscarConvocatorias() throws Exception 
	{
		return convocatoriaDAO.buscarConvocatorias();	
	}
	public void guardarConvocatoria(String idConvocatoria, String tipo, Integer año, Integer periodo, String etapa) throws Exception 
	{
		Convocatoria convocatoria= convocatoriaDAO.buscarConvocatoria(idConvocatoria);
    	if(convocatoria != null)
    	{
    		throw new Exception ("El Id: "+idConvocatoria+ " no está disponible");
    	}
    	Convocatoria c= new Convocatoria (idConvocatoria, tipo, año, periodo, etapa);
    	convocatoriaDAO.guardarConvocatoria(c);	
	}
	public void editarConvocatoria(Convocatoria c) throws Exception 
	{
		Convocatoria convocatoria= convocatoriaDAO.buscarConvocatoria(c.getIdConvocatoria());
		convocatoria.setTipo(c.getTipo());
		convocatoria.setAnio(c.getAnio());
		convocatoria.setPeriodo(c.getPeriodo());
		convocatoria.setEtapa(c.getEtapa());
	}
	public void eliminarConvocatoria(Convocatoria c) throws Exception 
	{
		Convocatoria convocatoria= convocatoriaDAO.buscarConvocatoria(c.getIdConvocatoria());
		convocatoriaDAO.eliminarConvocatoria(convocatoria);
	}
	
	
	//USUARIOS
	public List<Administrador> buscarUsuarios() throws Exception 
	{
		return usuarioDAO.buscarUsuarios();	
	}
	public List<Administrador> buscarUsuariosPorTipo(Integer tipo) throws Exception 
	{
		return usuarioDAO.buscarUsuariosPorTipo(tipo);
	}
	public void editaUsuario(Administrador u) throws Exception 
	{
		Administrador usuario = usuarioDAO.buscarUsuario(u.getUsuario());
		usuario.setUsuario(u.getUsuario());
		usuario.setNombre(u.getNombre());
		usuario.setCargo(u.getCargo());
		usuario.setDependencia(u.getDependencia());
	}
	public void eliminarUsuario(Administrador u) throws Exception 
	{
		Administrador usuario= usuarioDAO.buscarUsuario(u.getUsuario());
		usuarioDAO.eliminarUsuario(usuario);
	}
	public void guardarUsuario(String usuario, Integer tipo, String pass, String nombre, String cargo, String dependencia) throws Exception 
	{
		Administrador usuario2= usuarioDAO.buscarUsuario(usuario);
    	if(usuario2 != null){
    		throw new Exception ("El usuario: "+usuario+ " no está disponible");
    	}
    	
    	Administrador u= new Administrador (usuario, tipo, pass, nombre, cargo, dependencia);
    	usuarioDAO.guardarUsuario(u);
	}

	//CONCURSOS
	public List<Concurso> buscarConcursosPorConvocatoria(Convocatoria c) throws Exception 
	{
		return concursoDAO.buscarConcursoPorConvocatoria(c);
	}
	@Override
	public List<Concurso> buscarConcursosPorUnidad(Convocatoria c,UnidadAcademica u) throws Exception {
		
		return concursoDAO.buscarConcursoPorUnidad(c,u);
	}
	public List<Concurso> agruparConcursosPorFacultad(Convocatoria c) throws Exception 
	{
		return concursoDAO.agruparConcursoPorFacultad(c);
	}
	public List<Concurso> agruparConcursosPorUnidad(Convocatoria c) throws Exception 
	{
		return concursoDAO.agruparConcursoPorUnidad(c);
	}
	public void guardarConcurso(String idConcurso, String area, Convocatoria convocatoria, UnidadAcademica unidadAcademica)throws Exception 
	{
		Concurso concurso= concursoDAO.buscarConcurso(idConcurso);
    	if(concurso != null){
    		throw new Exception ("El id: "+idConcurso+ " no está disponible");
    	}
    	Concurso c= new Concurso (idConcurso, area, convocatoria, unidadAcademica);
    	concursoDAO.guardarConcurso(c);
	}
	public void editaConcurso(Concurso c) throws Exception
	{
		Concurso concurso = concursoDAO.buscarConcurso(c.getIdConcurso());
	    concurso.setArea(c.getArea());
	    concurso.setUnidadacademica(c.getUnidadAcademica());	
	}
	public void eliminarConcurso(Concurso concurso) throws Exception
	{
		Concurso c = concursoDAO.buscarConcurso(concurso.getIdConcurso());
		concursoDAO.eliminarConcurso(c);
	}
	
	//UNIDADES ACADEMICAS
	public List<UnidadAcademica> buscarUnidades() throws Exception 
	{
		return unidadAcademicaDAO.buscarUnidades();	
	}
	public UnidadAcademica buscarUnidadAcademica(Integer idUnidadAcademica) throws Exception
	{
		return unidadAcademicaDAO.buscarUnidadAcademica(idUnidadAcademica);
	}

	//ASPIRANTE
	public void guardarAspirante(Aspirante a) throws Exception {
		aspiranteDAO.guardarAspirante(a);
		
	}

	public Aspirante buscarAspirante(String idAspirante) throws Exception {
		
		return aspiranteDAO.buscarAspirante(idAspirante);
	}
	
		//INFORMACIÓN PERSONAL
		public InformacionPersonal buscarInfoPersonal(Aspirante a) throws Exception {
			
			return informacionPersonalDAO.buscarInfoPersonal(a);
		}
		//FORMACION ACADEMICA
		public List<FormacionAcademica> buscarFormAcademica(Aspirante a)throws Exception {
			
			return formacionAcademicaDAO.buscarFormAcademica(a);
		}
		//SEGUNDA LENGUA
		@Override
		public List<SegundaLengua> buscarIdiomas(Aspirante a) throws Exception {
			
			return segundaLenguaDAO.buscarIdiomas(a);
		}
		//EXPERIENCIA DOCENTE
		@Override
		public List<ExperienciaDocente> buscarExperienciaDocente(Aspirante a) throws Exception {
			
			return experienciaDocenteDAO.buscarExperienciaDocente(a);
		}
		//EXPERIENCIA PROFESIONAL
		@Override
		public List<ExperienciaProfesional> buscarExperienciaProfesional(Aspirante a) throws Exception {
			
			return experienciaProfesionalDAO.buscarExperienciaProfesional(a);
		}
		//PRODUCTIVIDAD INTELECTUAL
		@Override
		public List<ProductividadIntelectual> buscarProductividadIntelectual(Aspirante a) throws Exception {
			
			return productividadIntelectualDAO.buscarProductividadIntelectual(a);
		}
	
	//INSCRIPCIONES 
	public List<Inscripcion> buscarInscripcionePorAspirante(Convocatoria c, Aspirante a) throws Exception {
		
		return inscripcionDAO.buscarInscripcionesPorAspirante(c,a);
	}
	@Override
	public List<Inscripcion> buscarInscripcionesPorConvocatoria(Convocatoria c) throws Exception {
		
		return inscripcionDAO.buscarInscripciones(c);
	}
	@Override
	public void editarInscripcion(Inscripcion i) throws Exception {
		
		Inscripcion inscripcion = inscripcionDAO.buscarInscripcion(i.getIdInscripcion());
		
		inscripcion.setRadico(i.getRadico());
		inscripcion.setFechaRadicado(i.getFechaRadicado());
		inscripcion.setNumero(i.getNumero());
		
	}

	

	
	
	

	
}

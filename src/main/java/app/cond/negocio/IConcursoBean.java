package app.cond.negocio;

import java.util.List;

import javax.ejb.Remote;

import app.cond.entidades.Administrador;
import app.cond.entidades.Aspirante;
import app.cond.entidades.Concurso;
import app.cond.entidades.Convocatoria;
import app.cond.entidades.ExperienciaDocente;
import app.cond.entidades.ExperienciaProfesional;
import app.cond.entidades.FormacionAcademica;
import app.cond.entidades.InformacionPersonal;
import app.cond.entidades.Inscripcion;
import app.cond.entidades.ProductividadIntelectual;
import app.cond.entidades.SegundaLengua;
import app.cond.entidades.UnidadAcademica;

@Remote
public interface IConcursoBean 
{
	//Iniciar Sesion
	public Integer validarUsuario(String usuario) throws Exception;
	public Integer validarPass(String usuario, String password) throws Exception;
	public Integer validarSesion(String usuario, Integer tipo) throws Exception;
	
	//Convocatorias
	public Convocatoria buscarConvocatoria(String idConvocatoria);
	public List<Convocatoria> buscarConvocatorias() throws Exception;
	public void guardarConvocatoria(String idConvocatoria, String tipo, Integer año, Integer periodo, String Etapa) throws Exception;
	public void editarConvocatoria(Convocatoria c) throws Exception;
	public void eliminarConvocatoria(Convocatoria c) throws Exception;
	
	//Usuarios
	public List<Administrador> buscarUsuarios() throws Exception;
	public List<Administrador> buscarUsuariosPorTipo(Integer tipo) throws Exception;
	public void guardarUsuario(String usuario, Integer tipo, String pass, String nombre, String cargo, String dependencia) throws Exception;
	public void editaUsuario(Administrador u) throws Exception;
	public void eliminarUsuario(Administrador u) throws Exception;

	//Concursos
	public List<Concurso> buscarConcursosPorConvocatoria(Convocatoria c) throws Exception;
	public List<Concurso> buscarConcursosPorUnidad(Convocatoria c, UnidadAcademica u) throws Exception;
	public List<Concurso> agruparConcursosPorFacultad(Convocatoria c) throws Exception;
	public List<Concurso> agruparConcursosPorUnidad(Convocatoria c) throws Exception;
	public void editaConcurso(Concurso c) throws Exception;
	public void guardarConcurso(String idConcurso, String area, Convocatoria convocatoria, UnidadAcademica unidadAcademica) throws Exception;
	public void eliminarConcurso(Concurso concurso) throws Exception;
	
	//UnidadesAcademicas
	public List<UnidadAcademica> buscarUnidades() throws Exception;
	public UnidadAcademica buscarUnidadAcademica(Integer idUnidadAcademica)throws Exception;
	
	//Aspirantes
	public void guardarAspirante(Aspirante a) throws Exception;
	public Aspirante buscarAspirante(String idAspirante) throws Exception;
	
		//Informacion Personal
		public InformacionPersonal buscarInfoPersonal(Aspirante a) throws Exception;
		
		//Formacion Academica
		public List<FormacionAcademica> buscarFormAcademica(Aspirante a) throws Exception;
		
		//Segunda Lengua
		public List<SegundaLengua> buscarIdiomas(Aspirante a) throws Exception;
		
		//Experiencia Docente
		public List<ExperienciaDocente> buscarExperienciaDocente(Aspirante a) throws Exception;
		
		//Experiencia Profesional
		public List<ExperienciaProfesional> buscarExperienciaProfesional(Aspirante a) throws Exception;
		
		//Productividad Intelectual
		public List<ProductividadIntelectual> buscarProductividadIntelectual(Aspirante a) throws Exception;
		
	
	//Inscripcionesnes
	public List<Inscripcion> buscarInscripcionePorAspirante(Convocatoria c, Aspirante a) throws Exception;
	public List<Inscripcion> buscarInscripcionesPorConvocatoria(Convocatoria c) throws Exception;
	public void editarInscripcion(Inscripcion i) throws Exception;
	
	
	
}

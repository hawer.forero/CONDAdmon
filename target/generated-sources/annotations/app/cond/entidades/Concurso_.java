package app.cond.entidades;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Concurso.class)
public abstract class Concurso_ {

	public static volatile SingularAttribute<Concurso, String> area;
	public static volatile ListAttribute<Concurso, Inscripcion> inscripcion;
	public static volatile SingularAttribute<Concurso, UnidadAcademica> unidadAcademica;
	public static volatile SingularAttribute<Concurso, Convocatoria> convocatoria;
	public static volatile SingularAttribute<Concurso, String> idConcurso;

}


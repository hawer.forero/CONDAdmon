package app.cond.entidades;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Inscripcion.class)
public abstract class Inscripcion_ {

	public static volatile SingularAttribute<Inscripcion, String> numero;
	public static volatile SingularAttribute<Inscripcion, Aspirante> aspirante;
	public static volatile SingularAttribute<Inscripcion, Integer> idInscripcion;
	public static volatile SingularAttribute<Inscripcion, String> fechaRadicado;
	public static volatile SingularAttribute<Inscripcion, Concurso> concurso;
	public static volatile SingularAttribute<Inscripcion, Date> fechaInscripcion;
	public static volatile SingularAttribute<Inscripcion, String> radico;

}


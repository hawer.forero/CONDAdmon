package app.cond.entidades;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Facultad.class)
public abstract class Facultad_ {

	public static volatile ListAttribute<Facultad, UnidadAcademica> unidadadesAcademicas;
	public static volatile SingularAttribute<Facultad, String> nombre;
	public static volatile SingularAttribute<Facultad, Integer> idFacultad;

}


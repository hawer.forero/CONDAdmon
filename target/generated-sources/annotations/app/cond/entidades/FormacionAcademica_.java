package app.cond.entidades;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FormacionAcademica.class)
public abstract class FormacionAcademica_ {

	public static volatile SingularAttribute<FormacionAcademica, String> modalidad;
	public static volatile SingularAttribute<FormacionAcademica, Aspirante> aspirante;
	public static volatile SingularAttribute<FormacionAcademica, Date> fechaGrado;
	public static volatile SingularAttribute<FormacionAcademica, String> graduado;
	public static volatile SingularAttribute<FormacionAcademica, String> titulo;
	public static volatile SingularAttribute<FormacionAcademica, String> semestresAprobados;
	public static volatile SingularAttribute<FormacionAcademica, String> numeroTarjeta;
	public static volatile SingularAttribute<FormacionAcademica, Integer> idFormacionAcademica;

}


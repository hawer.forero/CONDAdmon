package app.cond.entidades;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Convocatoria.class)
public abstract class Convocatoria_ {

	public static volatile SingularAttribute<Convocatoria, String> idConvocatoria;
	public static volatile SingularAttribute<Convocatoria, String> tipo;
	public static volatile SingularAttribute<Convocatoria, Integer> periodo;
	public static volatile ListAttribute<Convocatoria, Concurso> concursos;
	public static volatile SingularAttribute<Convocatoria, String> etapa;
	public static volatile SingularAttribute<Convocatoria, Integer> anio;

}


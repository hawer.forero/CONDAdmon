package app.cond.entidades;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProductividadIntelectual.class)
public abstract class ProductividadIntelectual_ {

	public static volatile SingularAttribute<ProductividadIntelectual, Integer> autoresTraduccion;
	public static volatile SingularAttribute<ProductividadIntelectual, String> editorial;
	public static volatile SingularAttribute<ProductividadIntelectual, Integer> tipo;
	public static volatile SingularAttribute<ProductividadIntelectual, Integer> numero;
	public static volatile SingularAttribute<ProductividadIntelectual, Date> fechaPublicacionTraducccion;
	public static volatile SingularAttribute<ProductividadIntelectual, Integer> volumen;
	public static volatile SingularAttribute<ProductividadIntelectual, String> categoria;
	public static volatile SingularAttribute<ProductividadIntelectual, String> isbn;
	public static volatile SingularAttribute<ProductividadIntelectual, String> nombreTraduccion;
	public static volatile SingularAttribute<ProductividadIntelectual, String> institucion;
	public static volatile SingularAttribute<ProductividadIntelectual, String> revista;
	public static volatile SingularAttribute<ProductividadIntelectual, Integer> autores;
	public static volatile SingularAttribute<ProductividadIntelectual, String> nombre;
	public static volatile SingularAttribute<ProductividadIntelectual, Integer> folio2;
	public static volatile SingularAttribute<ProductividadIntelectual, String> issn;
	public static volatile SingularAttribute<ProductividadIntelectual, Aspirante> aspirante;
	public static volatile SingularAttribute<ProductividadIntelectual, Integer> folio;
	public static volatile SingularAttribute<ProductividadIntelectual, Integer> idProductividadIntelectual;
	public static volatile SingularAttribute<ProductividadIntelectual, Date> fechaPublicacion;
	public static volatile SingularAttribute<ProductividadIntelectual, Date> fechaTerminacion;
	public static volatile SingularAttribute<ProductividadIntelectual, String> anio;
	public static volatile SingularAttribute<ProductividadIntelectual, String> editorialTraduccion;

}


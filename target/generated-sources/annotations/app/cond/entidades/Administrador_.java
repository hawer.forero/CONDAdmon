package app.cond.entidades;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Administrador.class)
public abstract class Administrador_ {

	public static volatile SingularAttribute<Administrador, Integer> tipo;
	public static volatile SingularAttribute<Administrador, String> pass;
	public static volatile SingularAttribute<Administrador, String> usuario;
	public static volatile SingularAttribute<Administrador, String> cargo;
	public static volatile SingularAttribute<Administrador, String> dependencia;
	public static volatile SingularAttribute<Administrador, String> nombre;

}


package app.cond.entidades;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InformacionPersonal.class)
public abstract class InformacionPersonal_ {

	public static volatile SingularAttribute<InformacionPersonal, String> barrio;
	public static volatile SingularAttribute<InformacionPersonal, String> munCorrespondencia;
	public static volatile SingularAttribute<InformacionPersonal, Date> fechaNacimiento;
	public static volatile SingularAttribute<InformacionPersonal, BigInteger> celular1;
	public static volatile SingularAttribute<InformacionPersonal, String> direccion;
	public static volatile SingularAttribute<InformacionPersonal, String> depCorrespondencia;
	public static volatile SingularAttribute<InformacionPersonal, String> idioma;
	public static volatile SingularAttribute<InformacionPersonal, String> nacionalidad;
	public static volatile SingularAttribute<InformacionPersonal, String> depNacimiento;
	public static volatile SingularAttribute<InformacionPersonal, Aspirante> aspirante;
	public static volatile SingularAttribute<InformacionPersonal, Integer> idInformacionPersonal;
	public static volatile SingularAttribute<InformacionPersonal, BigInteger> celular2;
	public static volatile SingularAttribute<InformacionPersonal, String> munNacimiento;
	public static volatile SingularAttribute<InformacionPersonal, String> paisCorrespondencia;
	public static volatile SingularAttribute<InformacionPersonal, String> paisNacimiento;
	public static volatile SingularAttribute<InformacionPersonal, String> sexo;
	public static volatile SingularAttribute<InformacionPersonal, Integer> telefono;

}


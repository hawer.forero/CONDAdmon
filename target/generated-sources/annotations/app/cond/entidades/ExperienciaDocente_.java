package app.cond.entidades;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ExperienciaDocente.class)
public abstract class ExperienciaDocente_ {

	public static volatile SingularAttribute<ExperienciaDocente, String> dedicacion;
	public static volatile SingularAttribute<ExperienciaDocente, Date> fechaVinculacion;
	public static volatile SingularAttribute<ExperienciaDocente, Integer> idExperienciaDocente;
	public static volatile SingularAttribute<ExperienciaDocente, Integer> semanasSemestre;
	public static volatile SingularAttribute<ExperienciaDocente, Aspirante> aspirante;
	public static volatile SingularAttribute<ExperienciaDocente, Integer> folio;
	public static volatile SingularAttribute<ExperienciaDocente, String> institucion;
	public static volatile SingularAttribute<ExperienciaDocente, Date> fechaRetiro;
	public static volatile SingularAttribute<ExperienciaDocente, Integer> horasSemana;
	public static volatile SingularAttribute<ExperienciaDocente, Integer> horasSemestre;

}


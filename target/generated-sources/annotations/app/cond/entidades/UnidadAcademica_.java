package app.cond.entidades;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UnidadAcademica.class)
public abstract class UnidadAcademica_ {

	public static volatile SingularAttribute<UnidadAcademica, Integer> idUnidadAcademica;
	public static volatile SingularAttribute<UnidadAcademica, String> nombre;
	public static volatile SingularAttribute<UnidadAcademica, Facultad> facultad;

}


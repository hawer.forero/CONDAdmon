package app.cond.entidades;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SegundaLengua.class)
public abstract class SegundaLengua_ {

	public static volatile SingularAttribute<SegundaLengua, String> escribe;
	public static volatile SingularAttribute<SegundaLengua, Aspirante> aspirante;
	public static volatile SingularAttribute<SegundaLengua, String> habla;
	public static volatile SingularAttribute<SegundaLengua, Integer> idSegundaLengua;
	public static volatile SingularAttribute<SegundaLengua, String> lee;
	public static volatile SingularAttribute<SegundaLengua, String> nombre;

}


package app.cond.entidades;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Aspirante.class)
public abstract class Aspirante_ {

	public static volatile SingularAttribute<Aspirante, String> apellido2;
	public static volatile SingularAttribute<Aspirante, String> tipoId;
	public static volatile SingularAttribute<Aspirante, String> pass;
	public static volatile SingularAttribute<Aspirante, String> apellido1;
	public static volatile ListAttribute<Aspirante, ProductividadIntelectual> productividadIntelectual;
	public static volatile SingularAttribute<Aspirante, String> nombre;
	public static volatile SingularAttribute<Aspirante, InformacionPersonal> informacionPersonal;
	public static volatile ListAttribute<Aspirante, FormacionAcademica> formacionAcademica;
	public static volatile ListAttribute<Aspirante, Inscripcion> inscripcion;
	public static volatile ListAttribute<Aspirante, ExperienciaDocente> experienciaDocente;
	public static volatile SingularAttribute<Aspirante, String> idAspirante;
	public static volatile ListAttribute<Aspirante, SegundaLengua> segundaLengua;
	public static volatile ListAttribute<Aspirante, ExperienciaProfesional> experienciaProfesional;
	public static volatile SingularAttribute<Aspirante, String> email;

}


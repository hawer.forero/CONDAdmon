package app.cond.entidades;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ExperienciaProfesional.class)
public abstract class ExperienciaProfesional_ {

	public static volatile SingularAttribute<ExperienciaProfesional, Date> fechaVinculacion;
	public static volatile SingularAttribute<ExperienciaProfesional, String> tipo;
	public static volatile SingularAttribute<ExperienciaProfesional, Integer> idExperienciaProfesional;
	public static volatile SingularAttribute<ExperienciaProfesional, String> tipoVinculacion;
	public static volatile SingularAttribute<ExperienciaProfesional, Aspirante> aspirante;
	public static volatile SingularAttribute<ExperienciaProfesional, Integer> folio;
	public static volatile SingularAttribute<ExperienciaProfesional, Date> fechaRetiro;
	public static volatile SingularAttribute<ExperienciaProfesional, String> cargo;
	public static volatile SingularAttribute<ExperienciaProfesional, String> empresa;

}

